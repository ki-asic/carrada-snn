#!/usr/bin/env python3
"""
Main description of the module.
"""
# Standard libraries
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
# Local libraries
from carrada_snn.cfar.oscfar import plot_tools 

mpl.rcParams['lines.markersize'] = 3
mpl.rcParams['legend.fontsize'] = 'small'
mpl.rcParams['font.size'] = 7

def print_npz(data):
    lst = data.files
    for item in lst:
        print(item)
        print(data[item])

def load_timesteps_and_conf_matrices(*paths):
    timesteps = []
    conf_matrices = []
    for path in paths:
        d = np.load(path, allow_pickle=True)
        timesteps.append(d['timesteps'])
        conf_matrices.append(d['conf_matrices'])
    timesteps = np.concatenate(timesteps)
    conf_matrices = np.concatenate(conf_matrices)

    ordered_indices = timesteps.argsort()
    ordered_timesteps = timesteps[ordered_indices]
    print(ordered_timesteps)
    ordered_conf_matrices = conf_matrices[ordered_indices]

    return ordered_timesteps, ordered_conf_matrices

def calc_sensitivity_and_precision(conf_matrices):
    sensitivity = [] 
    precision = [] 
    for conf_matrix in conf_matrices: 
        tn,fp,fn,tp = conf_matrix.ravel() 
        sensitivity.append(tp/(tp+fn)) 
        precision.append(tp/(tp+fp)) 
    return sensitivity, precision

def main(path_1, path_2, title):
    data_1 = np.load(path_1, allow_pickle=True)
    data_2 = np.load(path_2, allow_pickle=True)
    print_npz(data_1)
    print_npz(data_2)
    timesteps = np.hstack((data_1['timesteps'], data_2['timesteps']))
    ordered_indices = timesteps.argsort()
    ordered_timesteps = timesteps[ordered_indices]
    print(ordered_timesteps)

    conf_matrices = np.vstack((data_1["conf_matrices"], data_2["conf_matrices"]))
    ordered_conf_matrices = conf_matrices[ordered_indices]

    sensitivity = [] 
    precision = [] 
    tps = [] 
    fns = [] 
    fps = [] 
    for conf_matrix in ordered_conf_matrices: 
        tn,fp,fn,tp = conf_matrix.ravel() 
        sensitivity.append(tp/(tp+fn)) 
        precision.append(tp/(tp+fp)) 
        tps.append(int(tp)) 
        fps.append(int(fp)) 
        fns.append(int(fn)) 
        print("N detections (groundtruth):", tp+fn)
    
    fig = plot_tools.plot_metrics(ordered_timesteps, sensitivity, precision)
    # plt.plot(ordered_timesteps, sensitivity, label="Sensitivity")
    # plt.plot(ordered_timesteps, precision, label="Precision")
    # plt.legend(loc="lower right")
    plt.ylim(0.73,1.02) 
    # plt.table(cellText=[tps,fns,fps], 
    #         rowLabels=["TP", "FN", "FP"], loc="top") 
    plt.title(title) 
    plt.tight_layout()
    plt.show()
    plot_path = "./{}.pdf".format(title)
    fig.savefig(plot_path, dpi=300)

def ca_cfar_round_comparison():    
    labels = ["round down","round to nearest", "round up"]
    data_base_path =  "../carrada_snn/cfar/cfar_results/final_results/"
    data_fnames = [ ["ca_cfar_round_down_performance2.npz", "ca_cfar_round_down_final_low.npz"],
                    ["ca_cfar_round_nearest_performance2.npz", "ca_cfar_round_nearest_final_low.npz"],
                    ["ca_cfar_round_up_performance2.npz", "ca_cfar_round_up_final_low.npz"]]
    plot_fname = "ca_cfar_round_comparison.pdf"

    three_way_performance_figure(labels, data_base_path, data_fnames, plot_fname)

def os_cfar_comparison():    
    labels = ["amplitudes", "dB values", "dB values + delay"]
    data_base_path =  "../carrada_snn/cfar/cfar_results/final_results/"
    data_fnames = [ ["os_cfar_performance2.npz","os_cfar_performance_final_low.npz"],
                    ["os_cfar_db_performance3_part1.npz", "os_cfar_db_performance3_part2.npz"],
                    ["os_cfar_db_delay_performance3_part1.npz", "os_cfar_db_delay_performance3_part2.npz"]]
    plot_fname = "os_cfar_comparison.pdf"

    three_way_performance_figure(labels, data_base_path, data_fnames, plot_fname)

def ca_vs_os_vs_db_comparison():
    labels = ["CA-CFAR (round to nearest)", "OS-CFAR (amplitudes)", "OS-CFAR (db values + delay)"]
    data_base_path =  "../carrada_snn/cfar/cfar_results/final_results/"
    data_fnames = [["ca_cfar_round_nearest_performance2.npz", "ca_cfar_round_nearest_final_low.npz"],
                   ["os_cfar_performance2.npz","os_cfar_performance_final_low.npz"],
                   ["os_cfar_db_delay_performance3_part1.npz", "os_cfar_db_delay_performance3_part2.npz"]]

    plot_fname = "cfar_performance_comparison_3way.pdf"

    ylim = (0.74,1.01)
    three_way_performance_figure(labels, data_base_path, data_fnames, plot_fname, ylim)

def ca_vs_os_cfar_comparison():
    labels = ["CA-CFAR (round to nearest)", "OS-CFAR (db values + delay)"]
    data_base_path =  "../carrada_snn/cfar/cfar_results/final_results/"
    data_fnames = [["ca_cfar_round_nearest_performance2.npz", "ca_cfar_round_nearest_final_low.npz"],
                   ["os_cfar_db_delay_performance3_part1.npz", "os_cfar_db_delay_performance3_part2.npz"]]

    plot_fname = "cfar_performance_comparison.pdf"

    f = plt.figure(figsize=(5,2))
    ax1 = f.add_subplot(1,2,1)
    ax2 = f.add_subplot(1,2,2, sharey=ax1)
    
    base_path = Path(data_base_path)

    # 1. Axis
    ax = ax1
    fnames = [base_path/ fn for fn in data_fnames[0]]
    timesteps, conf_matrices = load_timesteps_and_conf_matrices(*fnames)
    sensitivity, precision = calc_sensitivity_and_precision(conf_matrices)
    ax.plot(timesteps, sensitivity, "-", label="Sensitivity")
    ax.plot(timesteps, precision, "-", label="Precision")
    ax.grid()
    ax.set_ylabel("Performance indicator")
    ax.set_xlabel("# of timesteps")
    ax.set_title(labels[0])
    ax.set_ylim(0.74,1.01)
    #ax.legend()
    ax.text(-0.1,1.05, "A", weight="bold", fontsize=10, transform=ax.transAxes)

    # 2. Axis
    ax = ax2
    fnames = [base_path/ fn for fn in data_fnames[1]]
    timesteps, conf_matrices = load_timesteps_and_conf_matrices(*fnames)
    sensitivity, precision = calc_sensitivity_and_precision(conf_matrices)
    ax.plot(timesteps, sensitivity, "-", label="Sensitivity")
    ax.plot(timesteps, precision, "-", label="Precision")
    ax.grid()
    ax.set_xlabel("# of timesteps")
    ax.set_title(labels[1])
    ax.legend()
    ax.text(-0.1,1.05, "B", weight="bold", fontsize=10, transform=ax.transAxes)

    plt.tight_layout()

    f.savefig(plot_fname, dpi=300)
    plt.show()



def three_way_performance_figure(labels, data_base_path, data_fnames, plot_fname, ylim=None):
    """
    create a figure with 3 subplots comparing the sensitivity and precision of spiking CFAR implementations
    
    common method for CA-CFAR and OS-CFAR plots

    Args:
      data_base_path: base path where to find the data
      data_fnames: list of lists of filenames (npz) with data within
        data_base_path. Outer list size should be 3
      labels: list of labels to use for the plot for subplot titles. Length
        should be 3.
      plot_fname: path of filename to save the result plot
    """

    assert len(labels) == 3
    assert len(data_fnames) == 3

    f = plt.figure(figsize=(6.9,2))
    ax1 = f.add_subplot(1,3,1)
    ax2 = f.add_subplot(1,3,2, sharey=ax1)
    ax3 = f.add_subplot(1,3,3, sharey=ax1)

    base_path = Path(data_base_path)

    # 1. Axis
    ax = ax1
    fnames = [base_path/ fn for fn in data_fnames[0]]
    timesteps, conf_matrices = load_timesteps_and_conf_matrices(*fnames)
    sensitivity, precision = calc_sensitivity_and_precision(conf_matrices)
    ax.plot(timesteps, sensitivity, "-", label="Sensitivity")
    ax.plot(timesteps, precision, "-", label="Precision")
    ax.grid()
    if ylim:
        ax.set_ylim(*ylim)
    ax.set_ylabel("Performance indicator")
    ax.set_title(labels[0])
    #ax.legend()
    ax.text(-0.1,1.05, "A", weight="bold", fontsize=10, transform=ax.transAxes)

    # 2. Axis
    ax = ax2
    fnames = [base_path/ fn for fn in data_fnames[1]]
    timesteps, conf_matrices = load_timesteps_and_conf_matrices(*fnames)
    sensitivity, precision = calc_sensitivity_and_precision(conf_matrices)
    ax.plot(timesteps, sensitivity, "-", label="Sensitivity")
    ax.plot(timesteps, precision, "-", label="Precision")
    ax.grid()
    ax.set_xlabel("# of timesteps")
    ax.set_title(labels[1])
    ax.legend()
    ax.text(-0.1,1.05, "B", weight="bold", fontsize=10, transform=ax.transAxes)

    # 3. Axis
    ax = ax3
    fnames = [base_path/ fn for fn in data_fnames[2]]
    timesteps, conf_matrices = load_timesteps_and_conf_matrices(*fnames)
    sensitivity, precision = calc_sensitivity_and_precision(conf_matrices)
    ax.plot(timesteps, sensitivity, "-", label="Sensitivity")
    ax.plot(timesteps, precision, "-", label="Precision")
    #ax.legend()
    ax.grid()
    ax.set_title(labels[2])
    ax.text(-0.1,1.05, "C", weight="bold", fontsize=10, transform=ax.transAxes)

    plt.tight_layout()

    f.savefig(plot_fname, dpi=300)
    plt.show()


if __name__ == "__main__":

    #ca_cfar_round_comparison()
    #ca_vs_os_cfar_comparison()
    #os_cfar_comparison()
    ca_vs_os_vs_db_comparison()
    exit(-1)

    ####### CA-CFAR #######
    #for rounding in ["nearest"]:
    for rounding in ["up", "down", "nearest"]:
        path_1 = "../carrada_snn/cfar/cfar_results/final_results/ca_cfar_round_{}_performance2.npz".format(rounding)
        path_2 = "../carrada_snn/cfar/cfar_results/final_results/ca_cfar_round_{}_final_low.npz".format(rounding)
        main(path_1, path_2, "CA-CFAR_rounding_{}".format(rounding))


    ####### OS-CFAR #######
    # amp experiment
    path_1 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_performance2.npz"
    path_2 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_performance_final_low.npz"
    main(path_1, path_2, "OS-CFAR_comparison")
    exit(-1)

    # dB experiment
    path_1 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_db_performance3_part1.npz"
    path_2 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_db_performance3_part2.npz"
    main(path_1, path_2, "dB OS-CFAR comparison")
    # dB+delay experiment
    path_1 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_db_delay_performance3_part1.npz"
    path_2 = "../carrada_snn/cfar/cfar_results/final_results/os_cfar_db_delay_performance3_part2.npz"
    main(path_1, path_2, "dB+delay OS-CFAR comparison")

