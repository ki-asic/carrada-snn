import logging
import json
import numpy as np 
from numpy.random import default_rng 
import matplotlib 
import matplotlib.pyplot as plt 
import matplotlib.patches as mpatches 
import pathlib 
import sklearn.metrics 
import time 
# Local libraries 
from carrada_snn import carrada_dataset 
from carrada_snn.cfar.oscfar import os_cfar 
from carrada_snn.cfar.oscfar import plot_tools
from carrada_snn.cfar import eval_spiking_cfar


def main():
    sequences = carrada_dataset.get_sequences()

    rd_s_with_objects = [] 
    rd_s_annotated_with_objects = []                                                         
    rd_fnames_with_objects = [] #np.empty(dtype=str)

    nframes = 500
    for s in sequences: 
        # load range-doppler  
        rd_s = carrada_dataset.load_range_doppler_frames(s) 
        rd_s_annotated  = carrada_dataset.load_range_doppler_annotations_sparse(s) 
        rd_fnames = carrada_dataset.load_range_doppler_fnames(s) 
        index_list = [] 
        for frame_nr in range(rd_s.shape[0]): 
            count = np.count_nonzero(rd_s_annotated[frame_nr, 1:]) 
            if count > 0: 
                index_list.append(frame_nr) 
        rd_s_with_objects.append(rd_s[index_list, :, :]) 
        rd_s_annotated_with_objects.append(rd_s_annotated[index_list, :, :]) 
        #rd_fnames_with_objects.append(rd_fnames[index_list])
        rd_fnames_with_objects = np.concatenate([rd_fnames_with_objects, rd_fnames[index_list]])
    rd_s_with_objects = np.vstack(rd_s_with_objects) 
    rd_s_annotated_with_objects = np.vstack(rd_s_annotated_with_objects)                     
    print(rd_fnames_with_objects)

    rd_s_with_objects = rd_s_with_objects[:nframes] 
    rd_annotated = rd_s_annotated_with_objects[:nframes]
    rd_fnames_with_objects = rd_fnames_with_objects[:nframes] 

    frame_nr = 456
    print(rd_fnames_with_objects[frame_nr])

    plot_single_frame_comparison(rd_s_with_objects[frame_nr])

def plot_single_frame_comparison(rd_map):
    fstats = pathlib.Path("../carrada_snn/cfar/cfar_results/rd_map_stats.npz")

    rd_stats = np.load(fstats) 
    rd_map = rd_map - rd_stats['rd_median'] 

    # select region:
    #rd_map = rd_map[:64,:]
    rd_map_amp = 10**(0.05*rd_map)


    cfar_conf_file = "../config/cfar_experiment.json" 
    with open(cfar_conf_file) as f: 
        cfar_config = json.load(f)

    #rd_map_annotated = np.moveaxis(rd_annotated[frame_nr,1:,:,:],0,-1)

    timesteps = cfar_config["general_params"]["timesteps"]
    timesteps = 250
    oscfar, oscfar_snn, os_conf_matrix = eval_spiking_cfar.get_cfar( 
                                    rd_map,
                                    None, #rd_map_annotated not used
                                    rd_map_amp,
                                    "os_cfar", 
                                    timesteps, 
                                    **cfar_config["oscfar_params"] 
                                    ) 

    cacfar, cacfar_snn, ca_conf_matrix = eval_spiking_cfar.get_cfar( 
                                    rd_map,
                                    None, #rd_map_annotated not used
                                    rd_map_amp, 
                                    "ca_cfar", 
                                    timesteps, 
                                    **cfar_config["cacfar_params"] 
                                    ) 

    os_colored_map = plot_tools.get_colored_diff_map(oscfar, oscfar_snn, 0.4)
    ca_colored_map = plot_tools.get_colored_diff_map(cacfar, cacfar_snn, 0.4)
    print(rd_map.shape)

    #plot_javier_style(rd_map, os_colored_map, ca_colored_map, timesteps)
    plot_bernhard_style(rd_map, os_colored_map, ca_colored_map, timesteps)

    tn,fp,fn,tp = ca_conf_matrix.ravel()
    print("Sensitivity CA-CFAR:", tp/(tp+fn))
    print("Precision CA-CFAR: ", tp/(tp+fp))
    tn,fp,fn,tp = os_conf_matrix.ravel()
    print("Sensitivity OS-CFAR:", tp/(tp+fn))
    print("Precision OS-CFAR: ", tp/(tp+fp))

def plot_javier_style(rd_map, os_colored_map, ca_colored_map, timesteps):
    ### Plotting
    fig, axes = plt.subplots(ncols=3, figsize=(18, 6))

    plot_tools.plot_map_2d(rd_map, show=False, title="RD map", ax=axes[0],
                            show_ylabel=True)
    plot_tools.plot_map_2d(os_colored_map, show=False, title="OS-CFAR",
                            ax=axes[1], show_xlabel=True)
    plot_tools.plot_map_2d(ca_colored_map, show=False, title="CA-CFAR",
                            ax=axes[2])

    tp_patch = mpatches.Patch(color=[0,1,0], label='true positive')
    fn_patch = mpatches.Patch(color=[1,1,0], label='false negative')
    fp_patch = mpatches.Patch(color=[1,0,0], label='false positive')
    axes[2].legend(handles=[tp_patch, fn_patch, fp_patch], loc=(0.7,0.7))

    plt.show()

def plot_bernhard_style(rd_map, os_colored_map, ca_colored_map, timesteps):
    f = plt.figure(figsize=(6.9,2.5))
    ax1 = f.add_subplot(1,3,1)
    ax2 = f.add_subplot(1,3,2, sharey=ax1)
    ax3 = f.add_subplot(1,3,3, sharey=ax1)

    if True:
        rd_map = rd_map[:64,:]
        os_colored_map = os_colored_map[:64,:]
        ca_colored_map = ca_colored_map[:64,:]

    # Carrada stats
    v_max = 13.6
    v_min = -v_max
    r_min = 0
    r_max = 64*0.2

    aspect = (v_max-v_min)/(r_max - r_min)

    im = ax1.imshow(rd_map, origin='lower', extent=[v_min, v_max, r_min, r_max], aspect=aspect)
    #f.colorbar(im,ax=ax1)
    ax1.set_title("RD map")
    ax1.set_ylabel("range [m]")
    #ax1.set_xlabel("Doppler bin")
    
    ax2.imshow(os_colored_map, origin='lower', extent=[v_min, v_max, r_min, r_max], aspect=aspect)
    ax2.set_title("OS-CFAR ({} time steps)".format(timesteps))
    ax2.set_xlabel("rel. velocity [m/s]")

    ax3.imshow(ca_colored_map, origin='lower', extent=[v_min, v_max, r_min, r_max], aspect=aspect)
    ax3.set_title("CA-CFAR ({} time steps)".format(timesteps))
    #ax3.set_xlabel("Doppler bin")

    tp_patch = mpatches.Patch(color=[0,1,0], label='true positive')
    fn_patch = mpatches.Patch(color=[1,1,0], label='false negative')
    fp_patch = mpatches.Patch(color=[1,0,0], label='false positive')

    ax3.legend(handles=[tp_patch, fn_patch, fp_patch], loc="upper right", handlelength=1)

    ax1.text(-0.1,1.05, "A", weight="bold", fontsize=10, transform=ax1.transAxes)
    ax2.text(-0.1,1.05, "B", weight="bold", fontsize=10, transform=ax2.transAxes)
    ax3.text(-0.1,1.05, "C", weight="bold", fontsize=10, transform=ax3.transAxes)

    f.savefig("CFAR_single_frame_comparison.pdf", dpi=300)
    plt.show()

def main2():
    fname = "/media/hdd/carrada_dataset/Carrada/2019-09-16-12-55-51/range_doppler_numpy/000438.npy"
    rd_map2 = np.load(fname)
    plot_single_frame_comparison(rd_map2)


if __name__ == "__main__":
    #main()
    main2()
