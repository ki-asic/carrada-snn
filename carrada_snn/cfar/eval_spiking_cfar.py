#!/usr/bin/env python3
"""
Script to evaluate the spiking CFAR accuracy

To-Do:
    - add reference value to dB to power/amplitude converters
    - add the following parameters to the timestep sweep:
      - rounding scheme
      - use amplitude or power value 
      - option to subtract median value from each RD cell
"""
# Global libraries
import logging
import numpy as np
from numpy.random import default_rng
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import pathlib
import sklearn.metrics
import time
# Local libraries
from carrada_snn import carrada_dataset
from carrada_snn.cfar.oscfar import os_cfar
from carrada_snn.cfar.oscfar import plot_tools

matplotlib.rcParams['lines.markersize'] = 3
matplotlib.rcParams['legend.fontsize'] = 'small'
matplotlib.rcParams['font.size'] = 7


logger = logging.getLogger("Carrada-SNN")


def dB_to_power(rd_map):
    '''
    converts a range Doppler map in decibel to power values
    '''
    return 10**(0.1*rd_map)

def power_to_dB(rd_map):
    '''
    converts a range Doppler map from power to decibels
    '''
    return 10*np.log10(rd_map)


def dB_to_amplitude(rd_map):
    '''
    converts a range Doppler map in decibel to amplitude values
    '''
    return 10**(0.05*rd_map)

def amplitude_to_dB(rd_map):
    '''
    converts a range Doppler map from amplitudes to decibel
    '''
    return 20*np.log10(rd_map)

def convert_to_spike_times(rd_map, xmax=None, timesteps=1000, rounding="nearest"):
    """
    convert range-Doppler values (amplitudes) to spike times

    params:
    xmax - maximum value used for normalization. should be larger or equal than
           largest element of rd_map. If not provided the maximum of rd_map is taken.
    timesteps - number of timesteps for discretization
    rounding - rounding method. one of 'down', 'nearest' or 'up'

    returns tuple of (binned timesteps, xmax, timesteps)
    """
    if xmax is None:
        xmax = rd_map.max()
    t = (xmax-rd_map)/xmax
    if rounding == 'nearest':
        t_binned = np.round(t*timesteps, 0)
    elif rounding == 'down':
        t_binned = np.floor(t*timesteps)
    elif rounding == 'up':
        t_binned = np.ceil(t*timesteps)
    else:
        raise Exception("Unsupported rounding method")
    return t_binned, xmax, timesteps

def convert_to_rd_value(st_map, xmax, timesteps):
    """
    Convert spike times to rd map values
    """
    rd_map = xmax - st_map*xmax/timesteps
    return rd_map

def cfar_array(shape, target_list):
    """
    create a CFAR boolean arrray of the range-Doppler map shape out of target
    index list
    """
    rd_map_cfar = np.zeros(shape)
    for i,j,_ in target_list:
        rd_map_cfar[i,j]=1
    return rd_map_cfar

# copy-pasted  and modified from Chen's script
def ca_cfar_2d(rd_map, num_guard, num_train, alpha_in=5, out="list"):
    '''
    2 dimentional cfar
    range boundary expansion: mean value
    local maximum: guard region
    Doppler boundary expansion: other side value due to circularity.
    '''
    assert(num_guard%2==0)
    assert(num_train%2==0)
    rg_size, dp_size = rd_map.shape
    rd_map_bottom = np.min(rd_map)
    rd_map_mean = np.mean(rd_map)
    target_list = []
    far = 0.01   # false alarm rate
                    #far       #alpha 
                    # 0.1      2.59
                    # 0.01     5.85(P,axis=0)(V,axis=1)(C,axis=1)
                    # 0.001    9.95
                    # 0.0001  15.12
                    # 0.00001 21.62
    # num_train = 10 # both sides total
    num_half_train = num_train//2
    # num_guard = 2  # both sides total
    num_half_guard = num_guard//2
    num_side = num_half_train + num_half_guard
    train_cells = (num_train+num_guard+1)**2-(num_guard+1)**2

    #create expanded rdmap
    rdmap_ex = np.zeros((rg_size+num_train+num_guard,dp_size+num_train+num_guard))
    rdmap_ex[:num_side,:]=rd_map_mean 
    rdmap_ex[-num_side:,:]=rd_map_mean 
    rdmap_ex[num_side:-num_side,:num_side] = rd_map[:,-num_side:]
    rdmap_ex[num_side:-num_side,-num_side:] = rd_map[:,:num_side]
    rdmap_ex[num_side:num_side+rg_size,num_side:num_side+dp_size]=rd_map

    alpha = num_train*(far**(-1/num_train) - 1)
    # print('alpha=',alpha)
    alpha = alpha_in# we set alpha as a constant is a little bit tricky to match the data we use. For local region extraction the best value is 4 or 5.

    for i in range(num_side, rg_size + num_side): # Explore range not from zero
        for j in range(num_side, dp_size + num_side): # Explore Doppler not from zero
            sum1 = np.sum(rdmap_ex[i-num_side:i+num_side+1,j-num_side:j+num_side+1])
            sum2 = np.sum(rdmap_ex[i-num_half_guard:i+num_half_guard+1,j-num_half_guard:j+num_half_guard+1])
            p_noise = (sum1 - sum2) / train_cells # The guard cells are discarded

            threshold = alpha * p_noise
            if rdmap_ex[i,j] > threshold: 
                target_list.append([i-num_side,j-num_side,rdmap_ex[i,j]])

    if out == "list":
        return target_list # each item is (rg_idx, dp_idx, magnitude)
    else:
        return cfar_array(rd_map.shape, target_list)

# copy-pasted  and modified from Chen's script
def os_cfar_2d(rd_map, num_guard, num_train, k, scale_factor, out="list"):
    '''
    2 dimensional OS CFAR
    range boundary expansCFARion: mean value
    local maximum: guard region
    Doppler boundary expansion: other side value due to circularity.

    @param k: k-th largest value to find for statistical measure (int)
    @param scale_factor: how to scale CUT before compring with statistical measure
    '''
    assert(num_guard%2==0)
    assert(num_train%2==0)
    rg_size, dp_size = rd_map.shape
    rd_map_bottom = np.min(rd_map)
    rd_map_mean = np.mean(rd_map)
    target_list = []
    num_half_train = num_train//2
    num_half_guard = num_guard//2
    num_side = num_half_train + num_half_guard
    N_train_cells = (num_train+num_guard+1)**2-(num_guard+1)**2

    #create expanded rdmap
    rdmap_ex = np.zeros((rg_size+num_train+num_guard,dp_size+num_train+num_guard))
    rdmap_ex[:num_side,:]=rd_map_mean 
    rdmap_ex[-num_side:,:]=rd_map_mean 
    rdmap_ex[num_side:-num_side,:num_side] = rd_map[:,-num_side:]
    rdmap_ex[num_side:-num_side,-num_side:] = rd_map[:,:num_side]
    rdmap_ex[num_side:num_side+rg_size,num_side:num_side+dp_size]=rd_map

    for i in range(num_side, rg_size + num_side): # Explore range not from zero
        for j in range(num_side, dp_size + num_side): # Explore Doppler not from zero
            train_cells = np.concatenate((rdmap_ex[i-num_side:i-num_half_guard,j-num_side:j+num_side+1].ravel(),            \
                                         rdmap_ex[i+num_half_guard+1:i+num_side+1,j-num_side:j+num_side+1].ravel(),         \
                                         rdmap_ex[i-num_half_guard:i+num_half_guard+1,j-num_side:j-num_half_guard].ravel(), \
                                         rdmap_ex[i-num_half_guard:i+num_half_guard+1,j+num_half_guard+1:j+num_side+1].ravel()))
            # count how many training cells lower than the equivalent threshold and compare with k
            th_equival = rdmap_ex[i,j]*scale_factor
            smaller = np.count_nonzero(train_cells < th_equival)
            if smaller >= N_train_cells - k + 1:
                target_list.append([i-num_side,j-num_side,rdmap_ex[i,j]])

    if out == "list":
        return target_list # each item is (rg_idx, dp_idx, magnitude)
    else:
        return cfar_array(rd_map.shape, target_list)

def plot_range_doppler_and_cfar(rd, rd_cfar, rd_cfar_spiking):
    f = plt.figure()
    ax1 = f.add_subplot(1,2,1)
    ax2 = f.add_subplot(1,2,2, sharey=ax1)
    im = ax1.imshow(rd, origin="lower")
    cb = f.colorbar(im,ax=ax1)
    cb.set_label("Magnitude [dB]")

    ax1.set_title("RD map")
    ax1.set_ylabel("Range bin")
    ax1.set_xlabel("Doppler bin")

    green = [0,1,0]
    yellow = [1,1,0]
    red = [1,0,0]
    # True positive: green
    # False negative: yellow
    # False positive: red 
    rd_colored = np.zeros(shape=(rd_cfar.shape[0], rd_cfar.shape[1],3))

    # True positive: green
    rd_colored [ np.logical_and(rd_cfar,rd_cfar_spiking) ] = green

    # False negative
    rd_colored [ np.logical_and(rd_cfar,np.logical_not(rd_cfar_spiking)) ] = yellow

    # False positive
    rd_colored [ np.logical_and(np.logical_not(rd_cfar),rd_cfar_spiking) ] = red
    ax2.imshow(rd_colored, origin="lower")
    ax2.set_title("CFAR")

    tp_patch = mpatches.Patch(color=green, label='true positive')
    fn_patch = mpatches.Patch(color=yellow, label='false negative')
    fp_patch = mpatches.Patch(color=red, label='false positive')

    ax2.legend(handles=[tp_patch, fn_patch, fp_patch], loc=(0.1,0.7))
    ax2.set_xlabel("Doppler bin")
    ax1.set_ylabel("Range bin")
    f.savefig("rd_map_and_cfar.pdf")

def add_annotation_legend(ax):
    """
    add legend to annotation axis
    """
    red = [1,0,0]
    green = [0,1,0]
    blue = [0,0,1]
    c1_patch = mpatches.Patch(color=red, label=carrada_dataset.classes[1])
    c2_patch = mpatches.Patch(color=green, label=carrada_dataset.classes[2])
    c3_patch = mpatches.Patch(color=blue, label=carrada_dataset.classes[3])

    ax.legend(handles=[c1_patch, c2_patch, c3_patch])

def plot_range_doppler_and_annotation(rd,rd_an):
    f = plt.figure()
    ax1 = f.add_subplot(1,2,1)
    ax2 = f.add_subplot(1,2,2, sharey=ax1)

    im = ax1.imshow(rd, origin='lower')
    f.colorbar(im,ax=ax1)
    ax1.set_title("RD map")
    ax1.set_ylabel("Range bin")
    ax1.set_xlabel("Doppler bin")

    ax2.imshow(rd_an, origin='lower')
    ax2.set_title("Annotation")
    ax2.set_xlabel("Doppler bin")
    add_annotation_legend(ax2)



def plot_range_doppler_and_annotation_and_cfar(rd,rd_an, rd_cfar):
    f = plt.figure()
    ax1 = f.add_subplot(1,3,1)
    im = ax1.imshow(rd, origin='lower')
    f.colorbar(im,ax=ax1)
    ax1.set_title("RD map")
    ax1.set_ylabel("Range bin")
    ax1.set_xlabel("Doppler bin")
    
    ax2 = f.add_subplot(1,3,2, sharey=ax1)
    ax2.imshow(rd_an, origin='lower')
    ax2.set_title("Annotation")
    ax1.set_xlabel("Doppler bin")
    add_annotation_legend(ax2)

    ax3 = f.add_subplot(1,3,3, sharey=ax1)
    ax3.imshow(rd_cfar, origin='lower')
    ax3.set_title("CFAR")
    ax1.set_xlabel("Doppler bin")


def plot_maps(maps, labels):
    assert(len(maps) == len(labels))
    n_subplots = len(maps)

    f = plt.figure()

    for i in range(n_subplots):
        ax = f.add_subplot(1,n_subplots,i+1)
        im = ax.imshow(maps[i], origin='lower')
        #f.colorbar(im,ax=ax)
        ax.set_title(labels[i])
        if i==0:
            ax.set_ylabel("Range bin")
        ax.set_xlabel("Doppler bin")

    plt.show()

def eval_spiking_cfar(rd_map, num_train, num_guard, alpha_in, xmax, timesteps):
    """
    compares a spiking CA cfar with limited number of timesteps to conventional
    CA CFAR

    input:
    rd_map - range Doppler map with amplitudes
    num_train - number of training cells in each dimension
    num_guard - number of guard cells in each dimension
    alpha_in - alpha threshold
    xmax - maximum amplitude used for normalization when creating spike times
    timesteps - number of timesteps used for spike times

    returns a confusion matrix
    """

    rd_times, _xmax, _timesteps = convert_to_spike_times(rd_map, xmax=xmax, timesteps=timesteps)
    rd_map_quantized = convert_to_rd_value(rd_times, xmax, timesteps)
    rd_cfar = ca_cfar_2d(rd_map, num_guard, num_train, alpha_in, out="array")
    rd_cfar_quantized = ca_cfar_2d(rd_map_quantized, num_guard, num_train, alpha_in, out="array")
    print("Targets normal CFAR:", np.count_nonzero(rd_cfar))
    print("Targets quantized CFAR:", np.count_nonzero(rd_cfar_quantized))

    return sklearn.metrics.confusion_matrix(rd_cfar.ravel(), rd_cfar_quantized.ravel(), labels=[0,1])

def sweep_timesteps(rd_maps, fname, xmax=None):
    """
    sweep number timesteps and compare spiking CA CFAR to reference.
    saves confusion matrix for each timestep value into a numpy binary file

    parameters:
    rd_maps - range doppler maps - nump.ndarray with 3 dimensions: (frames, range, doppler)
    fname - output file for saving the results
    xmax - global maximum value in dB used for scaling the spike times. If this
           parameter is not provided, the xmax will be computed individually
           for each range doppler map!

    """
    n_timesteps = [50,100,200,500,1000,2000,5000]
    #n_timesteps = [50,100,200,500]
    cfar_params = dict(
        num_train = 10,
        num_guard = 2,
        alpha_in = 10
        )
    if xmax is not None:
        xmax_amplitude = dB_to_amplitude(xmax)

    conf_matrices = []
    for nts in n_timesteps:
        print("Timesteps:", nts)
        conf_matrix_sum = np.zeros((2,2))
        for frame_nr in range(rd_maps.shape[0]):
            rd_map = rd_maps[frame_nr,:,:]
            rd_map_amplitude = dB_to_amplitude(rd_map)

            if xmax is None:
                xmax_ = rd_map_amplitude.max()
            else:
                xmax_ = xmax_amplitude
            confusion_matrix = eval_spiking_cfar(rd_map_amplitude, xmax=xmax_,
                    timesteps=nts, **cfar_params)
            conf_matrix_sum += confusion_matrix
        print(conf_matrix_sum)
        tn,fp,fn,tp = conf_matrix_sum.ravel()
        print("TP:", tp)
        print("FN:", fn)
        print("FP:", fp)
        conf_matrices.append(conf_matrix_sum)
    np.savez(fname, n_timesteps=n_timesteps, conf_matrices=conf_matrices,
            xmax=xmax, cfar_params=cfar_params)
        
def plot_spiking_cfar_results(fname):
    d = np.load(fname, allow_pickle=True)
    n_timesteps = d['n_timesteps']
    conf_matrices = d['conf_matrices']
    sensitivity = []
    precision = []
    for conf_matrix in conf_matrices:
        tn,fp,fn,tp = conf_matrix.ravel()
        sensitivity.append(tp/(tp+fn))
        precision.append(tp/(tp+fp))
    plt.plot(n_timesteps,sensitivity, "+-", label="Sensitivity")
    plt.plot(n_timesteps,precision, "x-", label="Precision")
    plt.legend()
    plt.xlabel("# of timesteps")
    #plt.ylim(0.9,1.0)
    plt.savefig("cfar_vs_timesteps.pdf")
    plt.show()

def analyze_data_set(fout):
    """
    analyse range-Doppler maps from dataset and write to numpy binary file

    fout - output file name
    """
    sequences = carrada_dataset.get_sequences()
    all_frames = []
    for s in sequences:
        all_frames.append(carrada_dataset.load_range_doppler_frames(s))
    all_frames = np.vstack(all_frames)

    global_min = np.min(all_frames)
    global_max = np.max(all_frames)
    rd_median = np.median(all_frames, axis=0)
    rd_mean = np.mean(all_frames, axis=0)
    rd_max = np.max(all_frames, axis=0)

    np.savez(fout, global_min=global_min, global_max=global_max,
            rd_median=rd_median, rd_mean=rd_mean, rd_max=rd_max)

def plot_rd_map_stats(fin, fout):
    """
    plot stats of range doppler maps

    params:
    fin - numpy binary file with stats
    fout - file name for saving the figure
    """
    d = np.load(fin)
    f = plt.figure()
    norm = matplotlib.colors.Normalize(vmin=d['global_min'], vmax=d['global_max'])
    ax1 = f.add_subplot(1,3,1)
    ax1.imshow(d['rd_mean'], origin='lower', norm=norm)
    ax1.set_title("Mean")
    ax1.set_xlabel("Doppler bin")
    ax1.set_ylabel("Range bin")
    
    ax2 = f.add_subplot(1,3,2, sharey=ax1)
    ax2.imshow(d['rd_median'], origin='lower', norm=norm)
    ax2.set_title("Median")
    ax2.set_xlabel("Doppler bin")

    ax3 = f.add_subplot(1,3,3, sharey=ax1)
    im = ax3.imshow(d['rd_max'], origin='lower', norm=norm)
    ax3.set_title("Max")
    cb = f.colorbar(im,ax=ax3)
    cb.set_label("Magnitude [dB]")
    ax3.set_xlabel("Doppler bin")
    f.suptitle("Analysis of range Doppler maps (all frames)")
    plt.savefig(fout)
    plt.show()


def preprocessing(nframes, frame_nr=0, remove_reflections=True):
    """
    load range-Doppler frames from 1st sequence 

    @param nframes: number of frames to load
    @param frame_nr: number of first frame to load
    @param remove_reflections: remove median value from each RD point 

    @return tuple of (rd_maps in dB, range-Doppler annotation map, rd_maps in amplitude)
    """

    logger.info("Loading and pre-processing CARRADA dataset")
    t1 = time.time()
    # load sequences from Carrada Dataset
    sequences = carrada_dataset.get_sequences()
    s1 = sequences[0]

    # load range-doppler 
    rd_s = carrada_dataset.load_range_doppler_frames(s1)
    rd_s_annotated  = carrada_dataset.load_range_doppler_annotations_sparse(s1)

    # Remove reflections at 0 m/s
    fstats = pathlib.Path("carrada_snn/cfar/cfar_results/rd_map_stats.npz")
    if remove_reflections:
        if not fstats.is_file():
            logger.info("Analyze dataset")
            analyze_data_set(fstats)

    rd_s = rd_s[frame_nr:frame_nr+nframes, :, :]

    # Remove static offset
    if remove_reflections:
        rd_stats = np.load(fstats)
        rd_s = rd_s - rd_stats['rd_median']
    rd_map_amplitudes = dB_to_amplitude(rd_s)
    t2 = time.time()
    logger.info("Dataset correctly loaded")
    logger.debug("Dataset load time: {:.3g}s".format(t2-t1))

    return rd_s, rd_s_annotated, rd_map_amplitudes

def preprocessing2(nframes, remove_reflections=True, random=False, nseqs=None):
    """
    load range-Doppler frames with objects

    @param nframes: number of frames to load
    @param remove_reflections: remove median value from each RD point 
    @param random: if True, randomize the loaded frames, otherwise the first
                   nframes are taken
    @param nseqs: number of sequences to load. should be max 30. If not
                 specified, all 30 sequences are loaded. Used to speed up
                 evaluation of a few radar frames.

    @return tuple of (rd_maps in dB, range-Doppler annotation map, rd_maps in amplitude)
    """
    logger.info("Loading and pre-processing CARRADA dataset")
    t1 = time.time()
    # load sequences from Carrada Dataset
    sequences = carrada_dataset.get_sequences()
    rd_s_with_objects = []
    rd_s_annotated_with_objects = []

    if nseqs is None:
        nseqs = len(sequences)

    for s in sequences[:nseqs]:
        # load range-doppler 
        rd_s = carrada_dataset.load_range_doppler_frames(s)
        rd_s_annotated  = carrada_dataset.load_range_doppler_annotations_sparse(s)
        index_list = []
        for frame_nr in range(rd_s.shape[0]):
            count = np.count_nonzero(rd_s_annotated[frame_nr,1:])
            if count > 0:
                index_list.append(frame_nr)
        rd_s_with_objects.append(rd_s[index_list,:,:])
        rd_s_annotated_with_objects.append(rd_s_annotated[index_list,:,:])
    rd_s_with_objects = np.vstack(rd_s_with_objects)
    rd_s_annotated_with_objects = np.vstack(rd_s_annotated_with_objects)

    # Remove reflections at 0 m/s
    fstats = pathlib.Path("carrada_snn/cfar/cfar_results/rd_map_stats.npz")
    if remove_reflections:
        if not fstats.is_file():
            logger.info("Analyze dataset")
            analyze_data_set(fstats)

    if random:
        rng = default_rng(seed=42)
        idxs = np.arange(rd_s_with_objects.shape[0], dtype=np.int64)
        rng.shuffle(idxs)
        rd_s_with_objects = rd_s_with_objects[idxs[:nframes]]
        rd_s_annotated_with_objects = rd_s_annotated_with_objects[idxs[:nframes]]
    else:
        rd_s_with_objects = rd_s_with_objects[:nframes]
        rd_s_annotated_with_objects = rd_s_annotated_with_objects[:nframes]

    # Remove static offset
    if remove_reflections:
        rd_stats = np.load(fstats)
        rd_s_with_objects = rd_s_with_objects - rd_stats['rd_median']
    rd_map_amplitudes = dB_to_amplitude(rd_s_with_objects)
    t2 = time.time()
    logger.info("Dataset correctly loaded")
    logger.debug("Dataset load time: {:.3g}s".format(t2-t1))

    return rd_s_with_objects, rd_s_annotated_with_objects, rd_map_amplitudes


# FIXME: next function mixes plotting and data-analysis. split into two
def postprocessing(rd_map, rd_map_annotated, rd_map_amplitude,
                   rd_cfar, rd_cfar2, plot=False):
    if plot:
        plot_range_doppler_and_annotation(rd_map, rd_map_annotated)
        plot_range_doppler_and_annotation_and_cfar(rd_map, rd_map_annotated,
                                                   rd_cfar)

    conf_matrix = sklearn.metrics.confusion_matrix(rd_cfar.ravel(),
                                                   rd_cfar2.ravel(),
                                                   labels=[0,1]
                                                  )
    tn,fp,fn,tp = conf_matrix.ravel()
    sensitivity = tp / (tp+fn)
    precision = tp / (tp+fp)

    if plot:
        plot_range_doppler_and_cfar(rd_map, rd_cfar, rd_cfar2)
        plt.show()
    return (sensitivity, precision)


def ca_cfar_snn(rd_map, steps, **kwargs):
    """
    Calculate the spiking CA-CFAR over the input range-Doppler map
    """
    params = {}
    for key in ["xmax", "rounding"]:
        if key in kwargs:
            params[key]=kwargs[key]

    # compare time conversion
    rd_times, xmax, timesteps = convert_to_spike_times(rd_map,
            timesteps=steps, **params)
    rd_map2 = convert_to_rd_value(rd_times, xmax, timesteps)
    return ca_cfar_pipeline(rd_map2, **kwargs)


def ca_cfar_pipeline(rd_map, **kwargs):
    """
    Calculate the CA-CFAR over the input range-Doppler map
    """
    params = {}
    for key in ["alpha_in", "num_guard", "num_train"]:
        if key in kwargs:
            params[key]=kwargs[key]

    rd_cfar = ca_cfar_2d(rd_map, out="array", **params)
    return rd_cfar

def os_cfar2_snn(rd_map, steps, **kwargs):
    """
    Calculate the spiking OS-CFAR over the input range-Doppler map

    Bernhard's version of OS CFAR
    """
    params = {}
    for key in ["xmax", "rounding"]:
        if key in kwargs:
            params[key]=kwargs[key]

    # compare time conversion
    rd_times, xmax, timesteps = convert_to_spike_times(rd_map,
            timesteps=steps, **params)
    rd_map2 = convert_to_rd_value(rd_times, xmax, timesteps)
    return os_cfar2_pipeline(rd_map2, **kwargs)


def os_cfar2_pipeline(rd_map, **kwargs):
    """
    Calculate the OS-CFAR over the input range-Doppler map

    Bernhard's version of OS CFAR
    """
    params = {}
    for key in ["scale_factor", "k", "num_guard", "num_train"]:
        if key in kwargs:
            params[key]=kwargs[key]

    rd_cfar = os_cfar_2d(rd_map, out="array", **params)
    return rd_cfar


def os_cfar_pipeline(rd_map, method="std", timesteps=100, **kwargs):
    """
    Calculate the OS-CFAR over the input range-Doppler map
    """
    # Update list of parameters with timestep and RD information
    t_max = kwargs["t_max"]
    t_min = kwargs["t_min"]
    t_step = (t_max-t_min) / timesteps
    params = kwargs
    params["t_step"] = t_step
    params["x_max"] = rd_map.max()
    params["x_min"] = rd_map.min()

    if method=="std":
        cfar = os_cfar.OSCFAR(**params)
    elif method=="snn":
        cfar = os_cfar.OSCFAR_SNN(**params)
    elif method=="dB":
        cfar = os_cfar.OSCFAR_DB(**params)
    elif method=="snn_dB":
        cfar = os_cfar.OSCFAR_SNN_DB(**params)
    cfar(rd_map)
    return cfar.results


def get_cfar(rd_map, rd_map_annotated, rd_map_amplitude, algorithm, steps=100,
             **kwargs):
    """
    Compute CFAR over the range-Doppler map
    """
    if algorithm=="os_cfar":
        cfar = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs)
        cfar_snn = os_cfar_pipeline(rd_map_amplitude, method="snn",
                                    timesteps=steps, **kwargs
                                   )
    elif algorithm=="os_cfar_db":
        cfar = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs)
        cfar_snn = os_cfar_pipeline(rd_map_amplitude, method="snn_dB",
                                    timesteps=steps, **kwargs
                                   )
    elif algorithm=="os_cfar_db_delay":
        kwargs["delay"] = True
        cfar = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs)
        cfar_snn = os_cfar_pipeline(rd_map_amplitude, method="snn_dB",
                                    timesteps=steps, **kwargs
                                   )
    elif algorithm=="ca_cfar":
        cfar = ca_cfar_pipeline(rd_map_amplitude, **kwargs)
        cfar_snn =ca_cfar_snn(rd_map_amplitude, steps, **kwargs)
    elif algorithm == "os_cfar2":
        cfar = os_cfar2_pipeline(rd_map_amplitude, **kwargs)
        cfar_snn = os_cfar2_snn(rd_map_amplitude, steps, **kwargs)
    else:
        raise ValueError("Unrecognized CFAR method: {}".format(algorithm))

    conf_matrix = sklearn.metrics.confusion_matrix(cfar.ravel(),
            cfar_snn.ravel(), labels=[0,1])

    return (cfar, cfar_snn, conf_matrix)


def performance_experiment(alg, fout, **kwargs):
    """
    Run performance comparison
    
    Compares spiking and conventional algorithm for a certain amount of frames
    and different number of timesteps.

    For each number of timesteps, calculates the accumulated confusion matrix
    where the CFAR output of the conventional approach is considered the
    groundtruth and the spiking approach the prediction.

    The results are saved into a numpy binary file.
    """
    # Read experiment parameters
    nframes = kwargs["general_params"]["nframes"]
    timesteps = kwargs["general_params"]["timesteps_list"]
    iterations = len(timesteps)

    if alg in ["os_cfar", "os_cfar_db", "os_cfar_db_delay"]:
        cfar_params = kwargs["oscfar_params"]
    elif alg=="ca_cfar":
        cfar_params = kwargs["cacfar_params"]
    elif alg=="os_cfar2":
        cfar_params = kwargs["oscfar2_params"]
    else:
        raise ValueError("Wrong algorithm: {}".format(alg))
    log_message = "Running performance test of spiking vs non-spiking CFAR:"
    log_message += "\n- Algorithm: {}".format(alg)
    log_message += "\n- Number of frames: {}".format(nframes)
    log_message += "\n- Number of iterations: {}".format(iterations)
    logger.info(log_message)

    # Read dataset and pre-process it
    rd_maps, rd_annotated, rd_map_amplitudes = preprocessing2(nframes, random=False)
    conf_matrices = []
    for ts in timesteps:
        logger.debug("Iterating dataset for {} timesteps".format(ts))
        conf_matrix_sum = np.zeros((2,2))
        for frame_nr in range(nframes):
            rd_map = rd_maps[frame_nr,:,:]
            rd_map_annotated = np.moveaxis(rd_annotated[frame_nr,1:,:,:],0,-1)
            _, _, conf_matrix = get_cfar(
                                  rd_map, rd_map_annotated,
                                  rd_map_amplitudes[frame_nr],
                                  alg,
                                  ts,
                                  **cfar_params
                                 )
            conf_matrix_sum += conf_matrix

        conf_matrices.append(conf_matrix_sum)

    # save results
    np.savez(fout, alg=alg, timesteps=timesteps, conf_matrices=conf_matrices,
            cfar_params=cfar_params)
    logger.info("Performance experiment done!")

def error_distribution_experiment(alg, **kwargs):
    # Read experiment parameters
    nframes = kwargs["general_params"]["nframes"]
    timesteps = kwargs["general_params"]["timesteps"]

    if alg=="os_cfar":
        cfar_params = kwargs["oscfar_params"]
    elif alg=="ca_cfar":
        cfar_params = kwargs["cacfar_params"]
    elif alg=="os_cfar2":
        cfar_params = kwargs["oscfar2_params"]
    else:
        raise ValueError("Wrong algorithm: {}".format(alg))
    log_message = "Running error distribution test:"
    log_message += "\n- Algorithm: {}".format(alg)
    log_message += "\n- Number of frames: {}".format(nframes)
    log_message += "\n- Number of timesteps: {}".format(timesteps)
    logger.info(log_message)

    # Read dataset and pre-process it
    rd_maps, rd_annotated, rd_map_amplitudes = preprocessing2(nframes, random=False)
    conf_matrices = []
    for frame_nr in range(nframes):
        logger.debug("Processing frame Nº {}".format(frame_nr))
        rd_map = rd_maps[frame_nr,:,:]
        rd_map_annotated = np.moveaxis(rd_annotated[frame_nr,1:,:,:],0,-1)
        _, _, conf_matrix = get_cfar(
                                rd_map, rd_map_annotated,
                                rd_map_amplitudes[frame_nr],
                                alg,
                                timesteps,
                                **cfar_params
                                )
        conf_matrices.append(conf_matrix)
    # np.savez(fout, alg=alg, timesteps=timesteps, conf_matrices=conf_matrices,
    #          cfar_params=cfar_params, frame_nr=frame_nr, nframes=nframes)
    logger.info("Error analysis experiment done!")
    plot_error_dist_experiment(conf_matrices, cfar=alg)


def plot_error_dist_experiment(conf_matrices, cfar="os-cfar"):

    f_plot= "carrada_snn/cfar/cfar_results/{}_error_dist.pdf".format(cfar)
    logger.info("Plotting results and saving them in {}".format(f_plot))
    sensitivity = []
    precision = []
    tps = []
    fns = []
    fps = []
    for conf_matrix in conf_matrices:
        tn,fp,fn,tp = conf_matrix.ravel()
        if tp==0:
            continue
        sensitivity.append(tp/(tp+fn))
        precision.append(tp/(tp+fp))
        tps.append(int(tp))
        fps.append(int(fp))
        fns.append(int(fn))

    fig = plt.figure()
    plt.plot(np.linspace(1, len(sensitivity), len(sensitivity)), sensitivity, label="sensitivity")
    plt.plot(np.linspace(1, len(sensitivity), len(sensitivity)), precision, label="precision")
    plt.legend()
    #plt.title(alg, loc="left")
    plt.tight_layout()
    plt.show()
    fig.savefig(f_plot, dpi=50)
    logger.info("Plotting of error distribution experiment done!")


def plot_performance_experiment(f_results, plot_path):
    """
    Plot sensitivity and precision over time
    """
    logger.info("Plotting results and saving them in {}".format(plot_path))

    d = np.load(f_results, allow_pickle=True)
    timesteps = d['timesteps']
    conf_matrices = d['conf_matrices']
    alg = d['alg']

    sensitivity = []
    precision = []
    tps = []
    fns = []
    fps = []
    for conf_matrix in conf_matrices:
        tn,fp,fn,tp = conf_matrix.ravel()
        sensitivity.append(tp/(tp+fn))
        precision.append(tp/(tp+fp))
        tps.append(int(tp))
        fps.append(int(fp))
        fns.append(int(fn))

    fig = plot_tools.plot_metrics(timesteps, sensitivity, precision)
    plt.ylim(0.73,1.02)
    plt.table(cellText=[tps,fns,fps],
            rowLabels=["TP", "FN", "FP"], loc="top")
    #plt.title(alg, loc="left")
    plt.tight_layout()
    #plt.show()
    fig.savefig(plot_path, dpi=50)
    logger.info("Plotting of performance experiment done!")

def comparison_experiment(**kwargs):
    """
    Get the spiking and non-spiking CA and OS CFAR maps

    Plot the map containing false and true positives, and false and true
    negatives.
    """
    frame_nr = kwargs["general_params"]["frame_nr"]
    timesteps = kwargs["general_params"]["timesteps"]
    logger.info("Running comparison between spiking and non-spiking CFAR")
    rd_maps, rd_s_annotated, rd_map_amplitudes = preprocessing(nframes=1,
                                                               frame_nr=frame_nr
                                                              )
    rd_map = rd_maps[0, :, :]
    rd_map_amplitude = rd_map_amplitudes[0, :, :]
    rd_map_annotated = np.moveaxis(rd_s_annotated[frame_nr, 1:, :, :], 0, -1)

    # Compute the OS and CA CFAR, both spiking and non-spiking
    t_1 = time.time()
    oscfar, oscfar_snn, _ = get_cfar(rd_map, rd_map_annotated,
                                        rd_map_amplitude, "os_cfar", timesteps,
                                        **kwargs["oscfar_params"]
                                       )
    t_2 = time.time()
    cacfar, cacfar_snn, _ = get_cfar(rd_map, rd_map_annotated,
                                        rd_map_amplitude, "os_cfar2", timesteps,
                                        **kwargs["oscfar2_params"]
                                        #rd_map_amplitude, "ca_cfar", timesteps,
                                        #**kwargs["cacfar_params"]
                                       )
    t_3 = time.time()
    logger.debug("OS-CFAR experiment runtime: {:.3g}s".format(t_2-t_1))
    logger.debug("CA-CFAR experiment runtime: {:.3g}s".format(t_3-t_2))

    # Create a figure with the three main plots
    fig, axes = plt.subplots(ncols=3, figsize=(18, 6))
    os_colored_map = plot_tools.get_colored_diff_map(oscfar, oscfar_snn, 0.4)
    ca_colored_map = plot_tools.get_colored_diff_map(cacfar, cacfar_snn, 0.4)
    print(os_colored_map)

    plot_tools.plot_map_2d(rd_map, show=False, title="RD map", ax=axes[0],
                           show_ylabel=True)
    plot_tools.plot_map_2d(os_colored_map, show=False, title="OS-CFAR",
                           ax=axes[1], show_xlabel=True)
    plot_tools.plot_map_2d(ca_colored_map, show=False, title="CA-CFAR",
                           ax=axes[2])

    tp_patch = mpatches.Patch(color=[0,1,0], label='true positive')
    fn_patch = mpatches.Patch(color=[1,1,0], label='false negative')
    fp_patch = mpatches.Patch(color=[1,0,0], label='false positive')
    axes[2].legend(handles=[tp_patch, fn_patch, fp_patch], loc=(0.7,0.7))

    plot_path = "carrada_snn/cfar/cfar_results/cfar_result.pdf"
    logger.info("Plotting results and saving them in {}".format(plot_path))
    fig.savefig(plot_path, dpi=50)
    logger.info("Comparison experiment done!")

def compare_os_cfar_implementations(**kwargs):
    """
    Compare the two conventional implementations of OS CFAR

    Plot the map containing false and true positives, and false and true
    negatives.
    """
    frame_nr = kwargs["general_params"]["frame_nr"]
    timesteps = kwargs["general_params"]["timesteps"]
    logger.info("Running comparison between OS CFAR implementations")
    rd_maps, rd_s_annotated, rd_map_amplitudes = preprocessing(nframes=1,
                                                               frame_nr=frame_nr
                                                              )
    rd_map = rd_maps[0, :, :]
    rd_map_amplitude = rd_map_amplitudes[0, :, :]
    rd_map_annotated = np.moveaxis(rd_s_annotated[frame_nr, 1:, :, :], 0, -1)

    # Compute the OS and CA CFAR, both spiking and non-spiking
    t_1 = time.time()
    os_cfar_javier = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs['oscfar_params'])
    t_2 = time.time()
    os_cfar_db = os_cfar_pipeline(rd_map_amplitude, method="dB", **kwargs['oscfar_params'])
    t_3 = time.time()
    os_cfar_bernhard = os_cfar2_pipeline(rd_map_amplitude, method="std", **kwargs['oscfar2_params'])
    t_4 = time.time()
    logger.debug("OS-CFAR (Javier) experiment runtime: {:.3g}s".format(t_2-t_1))
    logger.debug("OS-CFAR (dB) experiment runtime: {:.3g}s".format(t_3-t_2))
    logger.debug("OS-CFAR (Bernhard) experiment runtime: {:.3g}s".format(t_4-t_3))

    logger.info("CFAR arrays are equal: {}".format(np.array_equal(os_cfar_javier, os_cfar_bernhard)))
    logger.info("CFAR arrays are equal: {}".format(np.array_equal(os_cfar_javier, os_cfar_db)))

    # Create a figure with the three main plots
    fig, axes = plt.subplots(ncols=3, figsize=(18, 6))

    plot_tools.plot_map_2d(rd_map, show=False, title="RD map", ax=axes[0],
                           show_ylabel=True)
    plot_tools.plot_map_2d(os_cfar_javier, show=False, title="OS-CFAR",
                           ax=axes[1], show_xlabel=True)
    plot_tools.plot_map_2d(os_cfar_bernhard, show=False, title="OS-CFAR2",
                           ax=axes[2])

    plot_path = "carrada_snn/cfar/cfar_results/os_cfar_result.pdf"
    logger.info("Plotting results and saving them in {}".format(plot_path))
    fig.savefig(plot_path, dpi=150)
    logger.info("Comparison experiment done!")

def analyze_cfar_statistics(algorithm, rd_map_amplitudes, rd_map_annotated, nframes, **kwargs):
    """
    analyze the output of various CFAR algorithms applied to RD-frames.

    Currently just sums up the number of detections for the algorithm and
    optionally saves them to a file.
    """

    n_detections = 0

    for frame_idx in range(nframes):
        rd_map_amplitude = rd_map_amplitudes[frame_idx]
        if algorithm=="os_cfar":
            cfar = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs['oscfar_params'])
        elif algorithm=="ca_cfar":
            cfar = ca_cfar_pipeline(rd_map_amplitude, **kwargs['cacfar_params'])
        elif algorithm == "os_cfar2":
            cfar = os_cfar2_pipeline(rd_map_amplitude, **kwargs['oscfar2_params'])
        elif algorithm == "groundtruth":
            cfar = rd_map_annotated[frame_idx,1:]
        else:
            raise ValueError("Unrecognized CFAR method: {}".format(algorithm))
        N = np.count_nonzero(cfar)
        n_detections += N

    algo2params = dict(os_cfar="oscfar_params", ca_cfar="cacfar_params", os_cfar2='oscfar2_params')
    if "fname" in kwargs:
        params = kwargs.get(algo2params[algorithm],{})
        np.savez(kwargs['fname'], nframes=nframes, n_detections=n_detections, params=params)
    print("algorithm:", algorithm)
    print("N_detections:", n_detections)
    print("N_detections/frame:", n_detections/nframes)


def calc_n_train(num_guard, num_train):
    """
    helper function to calculate number of training cells

    params:
        num_guard - number of guard cells in one dimension (sum of both sides)
        num_train - number of train cells in one dimension (sum of both sides)
    """
    return (num_train+num_guard+1)**2 - (num_guard+1)**2


def find_good_cfar_params():
    """
    Function to find OS CFAR params that create similar number of detections
    like an equivalent CA-CFAR implementation
    """

    nframes = 1000

    oscfar_params = {
            "scale_factor":0.2,
            "guarding_cells":3,
            "neighbour_cells":4,
            "k":36,
            "padding": "mean_toroid",
            "t_max": 1,
            "t_min": 0
        }

    cacfar_params = {
        "alpha_in": 5,
        "num_guard": 6,
        "num_train": 8,
        "rounding": "nearest"
    }

    N_train = calc_n_train(cacfar_params["num_guard"], cacfar_params["num_train"])
    print(N_train)
    kwargs = dict(oscfar_params=oscfar_params, cacfar_params=cacfar_params)

    # Load all necessary data (ONCE)
    rd_maps, rd_map_ann, rd_map_amp = preprocessing2(nframes, random=True)

    fname_base = pathlib.Path("carrada_snn/cfar/cfar_results/param_sweep")
    fname = fname_base / "ca_cfar.npz"
    analyze_cfar_statistics("ca_cfar", rd_map_amp, rd_map_ann, nframes, fname=fname, **kwargs)

    for k in range(40,60,2):
        fname = fname_base / "os_cfar_k={}.npz".format(k)
        print(fname)
        kwargs["oscfar_params"]["k"] = k
        analyze_cfar_statistics("os_cfar", rd_map_amp, rd_map_ann, nframes, fname=fname, **kwargs)

    #analyze_cfar_statistics("groundtruth", rd_map_amp, rd_map_ann, nframes, **kwargs)

def plot_cfar_and_annotations():
    """
    Quick implementation of function to plot the output of different CFAR
    functions together with RD-map and annotations.

    Used to verify the parameters found with function 'find_good_cfar_params'
    """

    nframes = 20
    timesteps = 500

    oscfar_params = {
            "scale_factor":0.2,
            "guarding_cells":3,
            "neighbour_cells":4,
            "k":44,
            "padding": "mean_toroid",
            "t_max": 1,
            "t_min": 0
        }

    cacfar_params = {
        "alpha_in": 5,
        "num_guard": 6,
        "num_train": 8,
        "rounding": "nearest"
    }

    N_train = calc_n_train(cacfar_params["num_guard"], cacfar_params["num_train"])
    print(N_train)

    kwargs = dict(oscfar_params=oscfar_params, cacfar_params=cacfar_params)

    # Load all necessary data (ONCE)
    rd_maps, rd_map_ann, rd_map_amp = preprocessing2(nframes, random=True)

    for frame_idx in range(rd_maps.shape[0]):
        rd_map_amplitude = rd_map_amp[frame_idx]

        algorithms = ["os_cfar", "ca_cfar", "groundtruth"]
        algorithms = ["os_cfar", "os_cfar_db", "os_cfar_snn", "os_cfar_snn_db"]
        cfars = []

        print("Frame", frame_idx)
        for algorithm in algorithms:
            if algorithm=="os_cfar":
                cfar = os_cfar_pipeline(rd_map_amplitude, method="std", **kwargs['oscfar_params'])
            elif algorithm=="os_cfar_snn":
                cfar = os_cfar_pipeline(rd_map_amplitude, method="snn", timesteps=timesteps, **kwargs['oscfar_params'])
            elif algorithm=="os_cfar_db":
                cfar = os_cfar_pipeline(rd_map_amplitude, method="dB", **kwargs['oscfar_params'])
            elif algorithm=="os_cfar_snn_db":
                cfar = os_cfar_pipeline(rd_map_amplitude, method="snn_dB", timesteps=timesteps, **kwargs['oscfar_params'])
            elif algorithm=="ca_cfar":
                cfar = ca_cfar_pipeline(rd_map_amplitude, **kwargs['cacfar_params'])
            elif algorithm == "os_cfar2":
                cfar = os_cfar2_pipeline(rd_map_amplitude, **kwargs['oscfar2_params'])
            elif algorithm == "groundtruth":
                #cfar = rd_map_ann[frame_idx,1:]
                cfar = np.moveaxis(rd_map_ann[frame_idx, 1:, :, :], 0, -1)
            else:
                raise ValueError("Unrecognized CFAR method: {}".format(algorithm))
            print(algorithm, "non-zeros:", np.count_nonzero(cfar))
            cfars.append(cfar)
        print("\n")
        algorithms.insert(0, "Amplitude")
        cfars.insert(0, rd_maps[frame_idx])
        #plot_maps(cfars, algorithms)

def analyze_data_quantization():
    """
    analyze how the values per RD-map are spread across different bins.
    Therefore, a histogram is taken.
    The number of bins can then be interpreted as the number of timesteps in the SNN case.
    """
    nframes = 20
    nseqs = 2
    frames_to_plot = [11]
    timesteps = 50

    oscfar_params = {
            "scale_factor":0.2,
            "guarding_cells":3,
            "neighbour_cells":4,
            "k":44,
            "padding": "mean_toroid",
            "t_max": 1,
            "t_min": 0
        }

    remove_offset = True

    rd_maps, rd_annotated, rd_map_amplitudes = preprocessing2(nframes, remove_reflections=remove_offset, random=True, nseqs=nseqs)

    for i in frames_to_plot:
        #for input_format in ["dB", "amp"]:
        f = plt.figure(figsize=(6.9,2)) # max width Frontiers style
        ax1 = f.add_subplot(1,2,1)
        ax2 = f.add_subplot(1,2,2, sharey=ax1)

        # Amplitude:
        ax1.hist(rd_map_amplitudes[i,:,:].flatten(), bins=timesteps)
        ax1.set_xlabel("Amplitude [a.u.]")
        ax1.set_ylabel("frequency")
        ax1.set_title("Histogram of amplitudes")
        ax1.text(0,1.05, "A", weight="bold", fontsize=10, transform=ax1.transAxes)

        # dB:
        ax2.hist(rd_maps[i,:,:].flatten(), bins=timesteps)
        ax2.set_xlabel("Power [dB]")
        ax2.set_title("Histogram of power values in decibel")
        ax2.text(0,1.05, "B", weight="bold", fontsize=10, transform=ax2.transAxes)
        
        plt.tight_layout()

        fname_base = pathlib.Path("carrada_snn/cfar/cfar_results")
        fname = fname_base/"hist_frame{}_timesteps{}.pdf".format(i,timesteps)
        print(fname)
        plt.savefig(fname, dpi=300)
        plt.show()
