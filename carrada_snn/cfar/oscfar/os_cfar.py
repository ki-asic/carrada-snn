#!/usr/bin/env python3
"""
Library containing CFAR implementations
"""
# Standard/3rd party libraries
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import numpy as np
from timeit import default_timer as timer
# Local libraries
from carrada_snn.cfar.oscfar import encoding
from carrada_snn.cfar.oscfar import if_neuron

def amplitude_to_dB(val):
    return 20*np.log10(val)

class TraditionalCFAR():
    """
    Implementation of the traditional CFAR algorithm. 
    """

    def __init__(self, scale_factor, guarding_cells, neighbour_cells, padding):
        """
        Initialization

        @param scale_factor: how to scale test_cell before compring with 
         statistical measure
        @param guarding_cells: number of guarding cells (counted from
         left to center)
        @param neighbour_cells: number of neighbour cells (as above)
        @param padding: str indicating how to compute CFAR at
         the borders (zero | mean | toroid | mean_toroid)
        """
        # more sense here
        self.name = 'generic cfar'
        self.processing_time = 0.

        # Store encoder parameteres
        self.scale_factor = scale_factor
        self.guarding_cells = guarding_cells
        self.neighbour_cells = neighbour_cells

        # Initialize arrays for input and output
        self.input_array = np.empty(1)
        self.results = np.empty(1)
        self.threshold = np.empty(1)
        
        # show threshold
        self.show_threshold = True

        # use zero padding 
        self.padding = padding

    def __call__(self, data, *args):
        """
        Calling the object executes the function 'run'.
        """
        return self.run(data, *args)
        
    def statistical_measure(self, np_array):
        """
        Computes the statistical measure for the CFAR algorithm.

        CACFAR: average; OSCFAR: k-th largest.
        """
        raise NotImplementedError("This method is for child classes")

    def compare(self, test_value, stat):
        """
        Compares the test_value against the statistical measure.

        It takes the scale_factor into consideration.

        @param test_value: value under consideration in CFAR 
        @param stat: statistical measuer computed from neighbours
        """
        return np.heaviside(self.scale_factor*test_value-stat, 0)

    def roi_1d(self, np_array):
        """
        Determines the 1D Region Of Interest

        e.g. the test cell and the neighbor values.

        @param np_array: np.array with ndim == 1
        """
        neighbours = np.empty(2*self.neighbour_cells)
        neighbours[:self.neighbour_cells] = np_array[:self.neighbour_cells]
        neighbours[self.neighbour_cells:] = np_array[self.neighbour_cells +
                                             2*self.guarding_cells + 1:]
        test_value = np_array[self.neighbour_cells+self.guarding_cells]
        return test_value, neighbours

    def roi_2d(self, np_array):
        """
        Determines the 2D Region Of Interest

        e.g. the test cell and the neighbor values.

        @param np_array: np.array with ndim == 2
        """
        no_neighbours = np_array.size - (2*self.guarding_cells+1)**2
        neighbours = np.empty(no_neighbours)

        # read out neighbours
        # TODO: This loop can probably be optimized by using np.where() function
        centre = 0
        for row in range(np_array.shape[0]):
            if row < self.neighbour_cells:
                chunk = np_array[row,:]
                neighbours[centre:centre+chunk.size] = chunk
                centre += chunk.size
            elif row < self.neighbour_cells+2*self.guarding_cells+1:
                chunk = np_array[row,:self.neighbour_cells]
                neighbours[centre:centre+chunk.size] = chunk
                centre += chunk.size
                chunk = np_array[row,
                                 self.neighbour_cells+2*self.guarding_cells+1:]
                neighbours[centre:centre+chunk.size] = chunk
                centre += chunk.size
            else:
                chunk = np_array[row,:]
                neighbours[centre:centre+chunk.size] = chunk
                centre += chunk.size

        if (centre != no_neighbours):
            error = """
            In roi_2d. centre ({}) does not add up to all neighbor cells ({}).
            """.format(centre,no_neighbours)
            raise ValueError(error)

        # test value can be found in the center
        test_value = np_array[self.neighbour_cells+self.guarding_cells,
                              self.neighbour_cells+self.guarding_cells]
        return test_value, neighbours

    def cfar_1d(self, np_array):
        """
        Applies the CFAR algorithm to a 1D array with a sliding window

        @param np_array: np.array with ndim == 1
        """
        # compute sizes related to sliding window 
        N = np_array.size
        window_size = 2*self.neighbour_cells + 2*self.guarding_cells + 1
        total_windows = N-window_size+1
        
        # initialize solution arrays
        self.results = np.zeros(total_windows)
        self.threshold = np.zeros_like(self.results)

        # iterate in sliding window fashion over input array
        for i in range(total_windows):
            # determine regions of interest
            test_value, neighbour_values = self.roi_1d(
                                            np_array[i:i+window_size])
            self.cfar_1d_core(i, test_value, neighbour_values)

        # return results array    
        return self.results

    def cfar_1d_core(self, i, test_value, neighbour_values):
        """
        Computes the statistical measure, threshold, and result

        @param i: which position of the result is considered
        @param test_value: test value
        @param neighbour_values: values of all neighbours
        """

        # compute statistical measure
        stat = self.statistical_measure(neighbour_values)
        # save threshold
        self.threshold[i] = stat / self.scale_factor
        # save results
        self.results[i] = self.compare(test_value,stat)
      
    def cfar_2d(self, np_array):
        """
        Applies the CFAR algorithm to a 2D array with a sliding window

        @param np_array: np.array with ndim == 2
        """
        # compute sizes related to sliding window 
        N_x, N_y = np_array.shape
        window_size = 2*self.neighbour_cells + 2*self.guarding_cells + 1
        total_windows_x = N_x - window_size + 1
        total_windows_y = N_y - window_size + 1
        
        # initialize solution arrays
        self.results = np.zeros(total_windows_x*total_windows_y)
        self.threshold = np.zeros_like(self.results)

        # iterate in sliding window fashion over input array
        for i,j in np.ndindex(total_windows_x,total_windows_y):
            # determine regions of interest
            test_value, neighbour_values = self.roi_2d(
                                            np_array[i:i+window_size,
                                                     j:j+window_size])
            self.cfar_1d_core(total_windows_y*i+j, test_value, neighbour_values)

        # reshape results
        self.results=self.results.reshape((total_windows_x,total_windows_y))
        self.threshold=self.threshold.reshape((total_windows_x,total_windows_y))

        # return results array    
        return self.results

    def apply_padding(self, rd_map, type="zero"):
        """
        Enlarge the 2D RD-map based on the specified method

        zero padding: Add the amount of zeros to the borders equivalent
            to the half size of the CFAR window
        mean padding: Fill the borders with the mean value of the
            corresponding row or column
        toroid padding: Fill the borders with the values of the RD-map
            at the other end of the specific row or column
        mean_toroid: Fill the borders by applying the mean technique in
            the range dimension, and the toroid technique in the Doppler
            dimension
        """
        # Create the base result array, with the original RD-map centered in an
        # all-zeros array.
        padding = self.guarding_cells + self.neighbour_cells
        n_rows, n_cols = rd_map.shape
        result = np.zeros((n_rows + 2*padding, n_cols + 2*padding))
        result[padding : padding+n_rows, padding : padding+n_cols] = rd_map
        if type=="zero":
            # Default option. The base result array stays unchanged
            pass
        elif type=="mean":
            # Row mean padding
            row_means = rd_map.mean(axis=1)
            for idx in range(padding):
                result[padding : padding+n_rows, idx] = row_means
                result[padding : padding+n_rows, padding+n_cols+idx] = row_means
            # Column mean padding, including prior row padding
            col_means = result[padding : padding+n_rows,:].mean(axis=0)
            for idx in range(padding):
                result[idx, :] = col_means
                result[padding+n_rows+idx , :] = col_means
        elif type=="toroid":
            # Row padding
            # Fetch the RD-map border areas and flip (mirror) them
            left_side = np.flip(rd_map[:, :padding], axis=1)
            right_side = np.flip(rd_map[:, -padding:], axis=1)
            # Apply to the padded map the opposite side of the original RD-maps
            result[padding:padding+n_rows, :padding] = right_side
            result[padding:padding+n_rows, -padding:] = left_side
            # Column padding
            # Same strategy, but including prior padded rows
            top_side = np.flip(result[padding:2*padding, :], axis=0)
            bottom_side = np.flip(result[-2*padding:-padding, :], axis=0)
            result[:padding, :] = bottom_side
            result[-padding:, :] = top_side
        elif type=="mean_toroid":
            # Row padding
            # Fetch the RD-map border areas and flip (mirror) them
            left_side = np.flip(rd_map[:, :padding], axis=1)
            right_side = np.flip(rd_map[:, -padding:], axis=1)
            # Apply to the padded map the opposite side of the original RD-maps
            result[padding:padding+n_rows, :padding] = right_side
            result[padding:padding+n_rows, -padding:] = left_side
            # Column mean padding, including prior row padding
            col_means = result[padding : padding+n_rows,:].mean(axis=0)
            for idx in range(padding):
                result[idx, :] = col_means
                result[padding+n_rows+idx , :] = col_means
        elif type in [None, False, ""]:
            # Restore the result array to the original RD-map
            result = rd_map
        else:
            raise ValueError("padding type not recognized: {}".format(type))
        return result

    def run(self, np_array):
        """
        Executes the CFAR algorithm on a given 1D or 2D array

        Function is executed when object is called.

        @param np_array: np.array with ndim == (1 or 2)
        """
        # Add padding to borders
        np_array = self.apply_padding(np_array, self.padding)

        self.input_array = np_array.copy()
        self.results = np.empty(1)

        # run CFAR algorithm.
        start = timer()
        if np_array.ndim == 1:
            self.cfar_1d(np_array)
        elif np_array.ndim == 2:
            self.cfar_2d(np_array)
        end = timer()
        self.processing_time = end-start

        return self.results


class OSCFAR(TraditionalCFAR):
    """
    Ordered Statistics CFAR algorithm
    """
    def __init__(self, scale_factor, guarding_cells, neighbour_cells, k,
                 padding, **kwargs):
        """
        Initialization

        @param scale_factor: how to scale test_cell before compring with 
                             statistical measure
        @param guarding_cells: number of guarding cells (counted from left to 
                               center)
        @param neighbour_cells: number of neighbour cells (as above)
        @param k: k-th largest value to find for statistical measure (int)
        @param zero_padding: bool indicating whether to compute CFAR at
         the borders by enlarging the frame with zeros
        """
        super(OSCFAR, self).__init__(scale_factor, guarding_cells,
                                    neighbour_cells, padding)
        # OSCFAR needs an integer k for determining the k-th largest value
        self.k = k
        self.name = 'OS-CFAR'

    def statistical_measure(self, np_array):
        return np.partition(np_array, -self.k)[-self.k]
        

class OSCFAR_SNN(TraditionalCFAR):
    """
    Ordered Statistics CFAR algorithms.
    """
    def __init__(self, scale_factor, guarding_cells, neighbour_cells,
                 k, padding, t_max, t_min, x_max, x_min, t_step=0.01):
        """
        Initialization.

        @param scale_factor: how to scale test_cell before compring with 
                             statistical measure
        @param guarding_cells: number of guarding cells (counted from left to 
                               center)
        @param neighbour_cells: number of neighbour cells (as above)
        @param k: k-th largest value to find for statistical measure (int)
        @param zero_padding: bool indicating whether to compute CFAR at
         the borders by enlarging the frame with zeros
        @param t_max: largest spike time for simulation
        @param t_min: smallest spike time for simulation
        @param x_max: upper bound for signal values
        @param x_min: lower bound for signal values
        """
        super(OSCFAR_SNN, self).__init__(scale_factor, guarding_cells,
                                        neighbour_cells, padding)
        self.name = 'OS-CFAR SNN'
        self.sim_time = 0.

        # OSCFAR needs an integer k for determining the k-th largest value
        self.k = k

        # SNN does not compute a threshold
        self.show_threshold = False

        # SNN uses time encoding
        self.t_max = t_max
        self.t_min = t_min
        self.x_max = x_max
        self.x_min = x_min
        self.t_step = t_step

    def cfar_1d_core(self, i, test_value, neighbour_values, delay=False):
        """
        Replace the parent class core by a functionally equivalent SNN

        See notebook for details.

        @param i: which position of the result is considered
        @param test_value: test value
        @param neighbour_values: values of all neighbours
        """
        # initialize time encoder
        time_encoder = encoding.TimeEncoder(t_max=self.t_max,t_min=self.t_min,
                                            x_max=self.x_max,x_min=self.x_min)
        # input spike times
        spike_times = np.zeros(neighbour_values.size+1)
        spike_times[:neighbour_values.size] = neighbour_values[:]
        spike_times[-1] = test_value*self.scale_factor
        spike_times=time_encoder(spike_times)
        if delay:
            spike_times[:neighbour_values.size] += self.t_step/2

        # define weights
        weights = np.ones(neighbour_values.size+1)
        weights *= -1
        weights[-1] = self.k
 
        # simulate IF neuron
        start_local = timer()
        res = if_neuron.simulate_IF_neuron(self.t_min, self.t_max, self.t_step,
                                           spike_times, weights
                                          )
        end_local = timer()
        self.sim_time += (end_local-start_local)
        
        # return 1 if spike occurs, 0 if not.
        self.results[i] = res

class OSCFAR_DB(OSCFAR):
    """
    Ordered Statistics CFAR algorithm in dB
    """
    def __init__(self, scale_factor, guarding_cells, neighbour_cells, k,
                 padding, **kwargs):
        """
        Initialization

        @param scale_factor: how to scale test_cell before compring with 
                             statistical measure
        @param guarding_cells: number of guarding cells (counted from left to 
                               center)
        @param neighbour_cells: number of neighbour cells (as above)
        @param k: k-th largest value to find for statistical measure (int)
        @param zero_padding: bool indicating whether to compute CFAR at
         the borders by enlarging the frame with zeros
        """
        super(OSCFAR_DB, self).__init__(scale_factor, guarding_cells,
                                    neighbour_cells, k, padding)
        self.name = 'OS-CFAR-DB'
        self.offset_dB = amplitude_to_dB(self.scale_factor)

    def statistical_measure(self, np_array):
        return np.partition(np_array, -self.k)[-self.k]
        
    def cfar_1d_core(self, i, test_value, neighbour_values):
        """
        Computes the statistical measure, threshold, and result

        @param i: which position of the result is considered
        @param test_value: test value
        @param neighbour_values: values of all neighbours
        """

        # compute statistical measure
        stat = self.statistical_measure(neighbour_values)
        # save threshold
        self.threshold[i] = stat / self.scale_factor
        # save results
        self.results[i] = self.compare(test_value,stat)
      

    def compare(self, test_value, stat):
        """
        Compares the test_value against the statistical measure.

        It takes the scale_factor into consideration.

        @param test_value: value under consideration in CFAR 
        @param stat: statistical measuer computed from neighbours
        """
        # compare in logscale
        return np.heaviside(amplitude_to_dB(test_value) + self.offset_dB - amplitude_to_dB(stat) , 0)

class OSCFAR_SNN_DB(OSCFAR_SNN):
    """
    Ordered Statistics CFAR algorithm with SNN with input in dB.
    """
    def __init__(self, scale_factor, guarding_cells, neighbour_cells,
                 k, padding, t_max, t_min, x_max, x_min, t_step=0.01,
                 delay=False):
        """
        Initialization.

        @param scale_factor: how to scale test_cell before compring with 
                             statistical measure
        @param guarding_cells: number of guarding cells (counted from left to 
                               center)
        @param neighbour_cells: number of neighbour cells (as above)
        @param k: k-th largest value to find for statistical measure (int)
        @param zero_padding: bool indicating whether to compute CFAR at
         the borders by enlarging the frame with zeros
        @param t_max: largest spike time for simulation
        @param t_min: smallest spike time for simulation
        @param x_max: upper bound for signal values
        @param x_min: lower bound for signal values
        """
        super(OSCFAR_SNN_DB, self).__init__(scale_factor, guarding_cells, neighbour_cells,
                 k, padding, t_max, t_min, x_max, x_min, t_step)

        self.name = 'OS-CFAR SNN DB'
        self.x_max_db = amplitude_to_dB(x_max)
        self.x_min_db = amplitude_to_dB(x_min)
        self.offset_dB = amplitude_to_dB(self.scale_factor)
        self.delay = delay

    def cfar_1d_core(self, i, test_value, neighbour_values):
        """
        Replace the parent class core by a functionally equivalent SNN

        See notebook for details.

        @param i: which position of the result is considered
        @param test_value: test value
        @param neighbour_values: values of all neighbours
        """

        # convert to dB
        test_value_dB = amplitude_to_dB(test_value)
        neighbour_values_dB = amplitude_to_dB(neighbour_values)

        # initialize time encoder
        time_encoder = encoding.TimeEncoder(t_max=self.t_max,t_min=self.t_min,
                                            x_max=self.x_max_db, x_min=self.x_min_db)
        # input spike times
        spike_times = np.zeros(neighbour_values_dB.size+1)
        spike_times[:neighbour_values_dB.size] = neighbour_values_dB[:]
        #spike_times[-1] = test_value*self.scale_factor
        spike_times[-1] = test_value_dB + self.offset_dB
        spike_times=time_encoder(spike_times)
        if self.delay:
            spike_times[:-1] += self.t_step/2

        # define weights
        weights = np.ones(neighbour_values_dB.size+1)
        weights *= -1
        weights[-1] = self.k
 
        # simulate IF neuron
        start_local = timer()
        res = if_neuron.simulate_IF_neuron(self.t_min, self.t_max, self.t_step,
                                           spike_times, weights
                                          )
        end_local = timer()
        self.sim_time += (end_local-start_local)
        
        # return 1 if spike occurs, 0 if not.
        self.results[i] = res
