#!/usr/bin/env python3
"""
Main description of the module.
"""
# Standard libraries
import matplotlib.pyplot as plt
import numpy as np


def format_plotting():
    # plt.rcParams['figure.figsize'] = (10, 4)
    #plt.rcParams['font.size'] = 20
    #    plt.rcParams['font.family'] = 'Times New Roman'
    plt.rcParams['axes.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['axes.titlesize'] = plt.rcParams['font.size']
    plt.rcParams['legend.fontsize'] = plt.rcParams['font.size']
    plt.rcParams['xtick.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['ytick.labelsize'] = plt.rcParams['font.size']
    # plt.rcParams['savefig.dpi'] = 1000
    plt.rcParams['savefig.format'] = 'eps'
    plt.rcParams['xtick.major.size'] = 3
    plt.rcParams['xtick.minor.size'] = 3
    plt.rcParams['xtick.major.width'] = 1
    plt.rcParams['xtick.minor.width'] = 1
    plt.rcParams['ytick.major.size'] = 3
    plt.rcParams['ytick.minor.size'] = 3
    plt.rcParams['ytick.major.width'] = 1
    plt.rcParams['ytick.minor.width'] = 1
    plt.rcParams['legend.frameon'] = True
    plt.rcParams['legend.loc'] = 'upper right'
    plt.rcParams['axes.linewidth'] = 1
    plt.rcParams['lines.linewidth'] = 1
    plt.rcParams['lines.markersize'] = 3

    plt.gca().spines['right'].set_color('none')
    plt.gca().spines['top'].set_color('none')
    plt.gca().xaxis.set_ticks_position('bottom')
    plt.gca().yaxis.set_ticks_position('left')
    return


def plot_map_2d(cfar, show=True, title="Spiking DFT", ax=None,
                show_xlabel=False, show_ylabel=False):
    """
    Visualize the 2D input and output data.
    """
    # Radar parameters
    f_0 = 77        # [GHz]
    c = 3 * 10**2   # [m/us]
    f_max = 24 / 2  # [GHz]
    S = 6.55        # [GHz/us]
    T_chirp = 54    # [us]
    n_chirps = 128
    # Calculate maximum range and velocity of the DFT spectrum
    wavelength = c*1000 / f_0
    d_max = (f_max*c) / (2*S)
    v_max = wavelength / (4*T_chirp)
    v_min = v_max / n_chirps

    # Plot output 2D CFAR
    if not ax:
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(6,6))
        plt.ylabel("Range (m)")
    else:
        fig = None
        if show_ylabel:
            ax.set_ylabel("Range (m)", fontsize=20)
        else:
            ax.set_yticks([])
    format_plotting()
    ax.imshow(cfar, extent=[-v_max, v_max-2*v_min, 0, d_max])
    ax.set_aspect("auto")
    if show_xlabel:
        ax.set_xlabel("Speed (m/s)", fontsize=20)
    ax.set_title(title)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    if show:
        plt.show()

    return fig, ax


def get_colored_diff_map(rd_cfar, rd_cfar_spiking, bg_alpha=0.1):
    # True positive: green
    # False negative: yellow
    # False positive: red 
    green = [0,1,0]
    yellow = [1,1,0]
    red = [1,0,0]
    rd_colored = np.ones(shape=(rd_cfar.shape[0], rd_cfar.shape[1],3))*bg_alpha

    # True positive: green
    rd_colored [ np.logical_and(rd_cfar,rd_cfar_spiking) ] = green

    # False negative
    rd_colored [ np.logical_and(rd_cfar,np.logical_not(rd_cfar_spiking)) ] = yellow

    # False positive
    rd_colored [ np.logical_and(np.logical_not(rd_cfar),rd_cfar_spiking) ] = red
    return rd_colored


def plot_metrics(timesteps, sensitivity, precision):
    format_plotting()
    fig = plt.figure()
    plt.plot(timesteps, sensitivity, "+-", label="Sensitivity")
    plt.plot(timesteps, precision, "x-", label="Precision")
    plt.legend(loc="lower right")
    plt.tight_layout()
    plt.xlabel("# of timesteps")
    plt.ylabel("Performance indicator")
    return fig
