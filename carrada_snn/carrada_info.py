import carrada_dataset

seq_paths = carrada_dataset.get_sequences()
print(f"Number of Scenarios: {len(seq_paths)}")
count = {'frames': 0, 1: 0, 2: 0, 3: 0}
for seq_path in seq_paths:
    # range_doppler_frames = carrada_dataset.load_range_doppler_frames(seq_path)
    range_doppler_annotations = carrada_dataset.load_range_doppler_annotations_box(seq_path)
    # range_angle_frames = carrada_dataset.load_range_angle_frames(seq_path)
    range_angle_annotations = carrada_dataset.load_range_angle_annotations_box(seq_path)
    for frame_id, annotation in range_doppler_annotations.items():
        count['frames'] += 1
        labels = annotation['labels']
        for label in labels:
            count[label] += 1
print(f"labeled-frames: {count['frames']}")
print(f"pedestrian-examples: {count[1]}")
print(f"bicycle-examples: {count[2]}")
print(f"car-examples: {count[3]}")
