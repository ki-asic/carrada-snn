import numpy as np
import visualization
import carrada_dataset

seq_paths = carrada_dataset.get_sequences()

seq_path = seq_paths[3]

camera_frames = carrada_dataset.load_camera_frames(seq_path)

range_doppler_frames = carrada_dataset.load_range_doppler_frames(seq_path)
range_doppler_annotations = carrada_dataset.load_range_doppler_annotations_box(seq_path)

range_angle_frames = carrada_dataset.load_range_angle_frames(seq_path)
range_angle_annotations = carrada_dataset.load_range_angle_annotations_box(seq_path)

print(np.shape(camera_frames))
print(np.shape(range_doppler_frames))
print(np.shape(range_angle_frames))
visualization.show_full_sequence(
    camera_frames, range_doppler_frames,
    range_angle_frames, range_doppler_annotations,
    range_angle_annotations)



