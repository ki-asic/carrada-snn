import os
import uuid
import datetime
import numpy as np
import random
import tensorflow as tf
import classifier_model_convSNN
import classifier_model_SNN
import classifier_dataset
from tensorboard.plugins.hparams import api as hp
from tf_profile import get_flops

dataset_path = 'tensorflow_dataset_unbalanced'
batch_size = 32
epochs = 100

train_dataset, val_dataset = classifier_dataset.get_spiking_dataset(dataset_path, batch_size)

# ----------------------------------------------------------
# train model on real valued data:

# reminder: to get a correct number of synaptic events after training, add "_real" to the end of the
# name-string of the tf-input (input_rd), then it will be ignored while counting input events.

# train_dataset, val_dataset = classifier_dataset.get_dataset(dataset_path, batch_size)
# input_shape = classifier_dataset.input_shape


# def reshape(x, y):
#    x = tf.reshape(x, (-1, input_shape[0], input_shape[1]*input_shape[2]))  # (Time, Y, X)
#    return x, y


# train_dataset = train_dataset.map(reshape)
# val_dataset = val_dataset.map(reshape)
# ----------------------------------------------------------


def get_number_of_spikes(model, dataset):
    n_spikes_network = 0
    n_spikes_encoding = 0
    n_synapse_updates = 0
    _n_spikes_prev_layer = 0
    extractor = tf.keras.Model(inputs=model.input, outputs=[layer.output for layer in model.layers])
    for sample in dataset:
        x = sample[0]
        extracted_out = extractor.predict(x)
        for layer, output in zip(extractor.layers, extracted_out):
            if 'Input' in str(type(layer)) and 'real' not in layer.name:
                n_spikes = tf.math.reduce_sum(output).numpy()
                n_spikes_encoding += n_spikes
                _n_spikes_prev_layer = n_spikes
            elif 'RNN' in str(type(layer)):
                if 'output' not in layer.cell.name and 'concat' not in layer.cell.name:
                    n_spikes = tf.math.reduce_sum(output[0]).numpy()
                    n_spikes_network += n_spikes
                    if 'conv' in layer.name.lower():
                        connectivity = np.prod(layer.cell.kernel_size)  # a good estimation?
                        n_synapse_updates += connectivity*_n_spikes_prev_layer
                    elif 'lif' in layer.cell.name:
                        connectivity = layer.cell.n_neurons
                        n_synapse_updates += connectivity * _n_spikes_prev_layer
                    else:
                        print(f'warning - no synaptic events added for layer: {layer.name}')
                    _n_spikes_prev_layer = n_spikes
                if 'output' in layer.cell.name:
                    connectivity = layer.cell.n_neurons
                    n_synapse_updates += connectivity * _n_spikes_prev_layer

    num_elements = tf.data.experimental.cardinality(dataset).numpy()*batch_size
    # num_elements -> number of samples in the dataset, spikes are accumulated over the whole set
    n_spikes_encoding = n_spikes_encoding / num_elements  # calculate mean spikes
    n_spikes_network = n_spikes_network / num_elements  # calculate mean spikes
    n_synapse_updates = n_synapse_updates / num_elements  # calculate mean spikes
    return n_spikes_encoding, n_spikes_network, n_synapse_updates


def train(log_dir, hparams):
    tf.random.set_seed(hparams[SEED])
    random.seed(hparams[SEED])
    os.environ['PYTHONHASHSEED'] = str(hparams[SEED])
    np.random.seed(hparams[SEED])

    #model = classifier_model_SNN.get_model()
    model = classifier_model_convSNN.get_model()

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1, profile_batch=0)
    callbacks = [tensorboard_callback]

    model.summary()
    get_flops(model)
    print("###############################")

    model.fit(
        train_dataset, validation_data=val_dataset,
        batch_size=batch_size, epochs=epochs,
        callbacks=callbacks, )

    _, accuracy = model.evaluate(val_dataset, batch_size=batch_size)
    n_spikes_encoding, n_spikes_network, n_synapse_updates = get_number_of_spikes(model, val_dataset)

    with tf.summary.create_file_writer(log_dir).as_default():
        hp.hparams(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)
        tf.summary.scalar(METRIC_NUM_SPIKES_NET, n_spikes_network, step=1)
        tf.summary.scalar(METRIC_NUM_SPIKES_ENC, n_spikes_encoding, step=1)
        tf.summary.scalar(METRIC_NUM_SYNAPSE_UPDATES, n_synapse_updates, step=1)


experiment_dir = 'logdir_' + datetime.datetime.now().strftime("%Y%m%d%H%M%S")
os.mkdir(experiment_dir)
with open(experiment_dir + '/dataset.txt', 'w') as f:
    f.write(dataset_path)

SEED = hp.HParam('seeds', hp.Discrete([42, 1337, 123, 666, 1000, 7, 948436, 13, 1408, 12, 69, 111111]))
METRIC_ACCURACY = 'accuracy'
METRIC_NUM_SPIKES_NET = 'num_spikes_network'
METRIC_NUM_SPIKES_ENC = 'num_spikes_encoding'
METRIC_NUM_SYNAPSE_UPDATES = 'num_synapse_updates'

with tf.summary.create_file_writer(experiment_dir).as_default():
    hp.hparams_config(hparams=[SEED],
                      metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy'),
                               hp.Metric(METRIC_NUM_SPIKES_NET, display_name='Num. S. Network'),
                               hp.Metric(METRIC_NUM_SPIKES_ENC, display_name='Num. S. Encoding'),
                               hp.Metric(METRIC_NUM_SYNAPSE_UPDATES, display_name='Num. Synaptic Updates')], )

for seed in SEED.domain.values:
    hparams = {SEED: seed}
    run_name = str(uuid.uuid4())
    print('--- Starting trial: %s' % run_name)
    print({h.name: hparams[h] for h in hparams})
    run_dir = experiment_dir + '/' + run_name
    train(run_dir, hparams)

print('done :)')
