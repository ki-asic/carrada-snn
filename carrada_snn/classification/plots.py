from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
from tensorboard.backend.event_processing.directory_watcher import DirectoryDeletedError
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np


def load(path: Path,
         scalars: bool = True
         ) -> dict:
    """

    :param path:
    :param scalars:
    :param hyperparameter:
    :return:
    """
    runs = {}
    for run_directory in path.iterdir():
        if not run_directory.is_dir():
            continue
        run_id = run_directory.name
        run_dict = {}
        try:
            if scalars:
                ea = EventAccumulator(str(path / run_id / 'validation')).Reload()
                scalar_dict = _get_scalars(ea)
                run_dict['scalars'] = scalar_dict
            runs[run_id] = run_dict
        except DirectoryDeletedError:
            print(f'ERROR: could not load run: {run_id}')
    return runs


def _get_scalars(ea: EventAccumulator) -> dict:
    tags = ea.Tags()
    scalars = tags['scalars']
    scalar_dict = {}
    for scalar in scalars:
        values, wall_time, steps = [], [], []
        for event in ea.Scalars(scalar):
            values.append(event.value)
            wall_time.append(event.wall_time)
            steps.append(event.step)
        scalar_dict[scalar] = {'values': values, 'wall_time': wall_time, 'steps': steps}
    return scalar_dict


def plot_stds(mean, label, std=None, c=(1, 0, 0)):
    x = list(range(len(mean)))
    plt.plot(x, mean, color=c, label=label)
    if std is not None:
        plt.fill_between(x, mean+std, mean-std,  facecolor=(c[0], c[1], c[2], 0.3), interpolate=False, alpha=0.3)


def get_accuracies(experiment, epochs):
    accuracies = []
    for id, run in experiment.items():
        accuracy = run['scalars']['epoch_accuracy']['values']
        if len(accuracy) < epochs:
            print(f'skip run: {id} (early-stopping)')
            continue
        accuracies.append(accuracy[:epochs])
    return np.array(accuracies)


if __name__ == '__main__':
    experiments = ['logdir_20210205164519']
    labels = ['unbalanced', 'balanced?']
    colors = [(238/255, 129/255, 60/255), (132/255, 182/255, 167/255)]
    experiments_path = Path(r'.')

    for i, experiment in enumerate(experiments):
        experiment_path = experiments_path / experiment
        data = load(experiment_path)
        accuracies = get_accuracies(data, 20)
        mean = np.mean(accuracies, axis=0)
        std = np.std(accuracies, axis=0)
        plot_stds(mean, label=labels[i], c=colors[i], std=std)
    plt.legend(loc='lower right')
    plt.show()
    # from pprint import pprint
    # pprint(experiment)
    # print('done')
