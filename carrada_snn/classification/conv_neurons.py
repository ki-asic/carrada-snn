import tensorflow as tf
from tensorflow.python.framework import tensor_shape
from tensorflow.python.keras.utils import conv_utils
import numpy as np


# CHECK AGAINST SPIKE ENCODED DATASET
# normal dataset to spiking convolution


@tf.custom_gradient
def spike_function(v_to_threshold):
    z = tf.cast(tf.greater(v_to_threshold, 1.), tf.float32)

    def grad(dy):
        return [dy * tf.maximum(1 - tf.abs(v_to_threshold - 1), 0)]
        # @negative: v_to_threshold < 0 -> dy*0
        # @rest: v_to_threshold = 0 -> dy*0+
        # @thresh: v_to_threshold = 1 -> dy*1
        # @+thresh: v_to_threshold > 1 -> dy*1-
        # @2thresh: v_to_threshold > 2 -> dy*0

    return z, grad


class Conv2DLifCell(tf.keras.layers.Conv2D):
    def __init__(self, filters, kernel_size, strides=(1, 1), use_bias=True, tau=20., threshold=0.1, decay_train_mode=1):
        super(Conv2DLifCell, self).__init__(filters, kernel_size, strides=strides,
                                            activation=None, use_bias=use_bias, padding='valid')

        self.spike_function = spike_function
        self.tau = tau
        self.decay = tf.cast(tf.exp(-1 / self.tau), self.dtype)
        self.threshold = threshold
        # 0 = no training, 1 = single value per layer, 2 = vector
        self.decay_train_mode = decay_train_mode

        self.kernel = None
        self.neurons = None  # we be calculated while building the layer

    def build(self, input_shape):
        if self.decay_train_mode == 0:
            self.decay = tf.Variable(initial_value=self.decay, trainable=False, dtype=self.dtype, name='decay')
        elif self.decay_train_mode == 1:
            self.decay = tf.Variable(initial_value=self.decay, trainable=True, dtype=self.dtype, name='decay')
        # elif self.decay_train_mode == 2:
        #    self.decay = tf.Variable(initial_value=decay * tf.ones((self.n_shifts_per_kernel, self.n_kernels)),
        #                            trainable=True, dtype=self.dtype, name='decay')
        else:
            print('Wrong decay train mode specified')
        super(Conv2DLifCell, self).build(input_shape)
        self.neurons = self.compute_output_shape(input_shape)

    @property
    def state_size(self):
        return self.neurons[1:], self.neurons[1:]

    def get_initial_state(self, inputs=None, batch_size=None, dtype=None):
        # del inputs  # Unused
        v = tf.zeros((batch_size, self.neurons[1], self.neurons[2], self.neurons[3]), dtype=dtype)
        z = tf.zeros((batch_size, self.neurons[1], self.neurons[2], self.neurons[3]), dtype=dtype)
        return v, z

    def call(self, input_at_t, states_at_t):
        old_v, old_z = states_at_t
        i_t = super(Conv2DLifCell, self).call(input_at_t)

        i_reset = old_z * self.threshold
        new_v = self.decay * old_v + (1.0 - self.decay) * i_t - i_reset
        new_z = spike_function(new_v / self.threshold)

        return new_z, (new_v, new_z)
