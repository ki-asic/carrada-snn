"""
To convert the CARRADA-Dataset to a tensorflow-format
"""
import tensorflow as tf
import numpy as np
#input_shape = (8, 18, 10)  # (Time, Y, X)
input_shape = (8, 26, 20)  # (Time, Y, X)
prepare_2d_data = False
normalize_en = True

# function for range dependent encoding/normalization
# v = m * v + n | v = cut_y if v < cut_y
m = -0.3
n = 65
cut_y = 38


def get_dataset(dataset_path, batch_size):
    shape_1 = input_shape[1:] if prepare_2d_data else input_shape
    shape_2 = 1 if prepare_2d_data else input_shape[0]
    dataset = tf.data.experimental.load(dataset_path,
                                        ((tf.TensorSpec(shape=shape_1, dtype=tf.float64, name=None),
                                        tf.TensorSpec(shape=(shape_2,), dtype=tf.int32, name=None)),
                                        tf.TensorSpec(shape=(), dtype=tf.float64, name=None)))
    num_elements = tf.data.experimental.cardinality(dataset).numpy()

    train_size = int(0.8 * num_elements)
    val_size = int(0.2 * num_elements)
    train_dataset = dataset.take(train_size)
    val_dataset = dataset.skip(train_size)
    val_dataset = val_dataset.take(val_size)

    def append_channel(x, y):
        x = tf.expand_dims(x[0], axis=-1)  # Channels last for 3DConv
        y = y - 1
        return x, y

    ranges = list(range(input_shape[1]))
    ranges = tf.repeat(tf.expand_dims(ranges, axis=-1), input_shape[2], axis=-1)
    ranges = tf.expand_dims(ranges, axis=0)

    def append_channel_normalization(x, y):
        x_shape = np.shape(x[0])
        r = tf.repeat(tf.expand_dims(x[1], axis=-1), x_shape[1], axis=-1)
        r = tf.repeat(tf.expand_dims(r, axis=-1), x_shape[2], axis=-1)
        r = tf.cast(r + ranges, tf.float64)
        v = m * r + n
        v = tf.where(tf.math.greater(v, r), v, cut_y)

        x = x[0]/v  # range dependent normalization
        x = tf.expand_dims(x, axis=-1)  # Channels last for 3DConv
        y = y - 1
        return x, y

    train_dataset = train_dataset.map(append_channel)
    val_dataset = val_dataset.map(append_channel)

    return train_dataset.batch(batch_size), val_dataset.batch(batch_size)


def get_spiking_dataset(dataset_path, batch_size):
    dataset = tf.data.experimental.load(dataset_path,
                                        ((tf.TensorSpec(shape=input_shape, dtype=tf.float64, name=None),
                                         tf.TensorSpec(shape=(input_shape[0],), dtype=tf.int32, name=None)),
                                         tf.TensorSpec(shape=(), dtype=tf.float64, name=None)))
    num_elements = tf.data.experimental.cardinality(dataset).numpy()

    train_size = int(0.8 * num_elements)
    val_size = int(0.2 * num_elements)
    train_dataset = dataset.take(train_size)
    val_dataset = dataset.skip(train_size)
    val_dataset = val_dataset.take(val_size)

    ranges = list(range(input_shape[1]))
    ranges = tf.repeat(tf.expand_dims(ranges, axis=-1), input_shape[2], axis=-1)
    ranges = tf.expand_dims(ranges, axis=0)

    def spikalize_const_threshold(x, y):
        x = tf.reshape(x[0], (input_shape[0], input_shape[1]*input_shape[2]))  # (Time, Y*X)
        x = x > 0.5  # <- dataset is not normalized yet... (binary encoding)
        y = y - 1
        return x, y

    def spikalize(x, y):
        x_shape = np.shape(x[0])

        r = tf.repeat(tf.expand_dims(x[1], axis=-1), x_shape[1], axis=-1)
        r = tf.repeat(tf.expand_dims(r, axis=-1), x_shape[2], axis=-1)
        r = tf.cast(r + ranges, tf.float64)
        thresholds = m * r + n
        thresholds = tf.where(tf.math.greater(thresholds, r), thresholds, cut_y)
        x = x[0] > thresholds
        x = tf.reshape(x, (input_shape[0], input_shape[1]*input_shape[2]))  # (Time, Y*X)
        # constant threshold:
        # x = x > 40  # <- dataset is not normalized yet... (binary encoding)
        # range dependent threshold:
        y = y - 1
        return x, y

    train_dataset = train_dataset.map(spikalize_const_threshold)
    val_dataset = val_dataset.map(spikalize_const_threshold)
    return train_dataset.batch(batch_size), val_dataset.batch(batch_size)

def normalize(data):
    for i, roi in enumerate(data):
        data[i] = (data[i]-np.min(data[i]))/(np.max(data[i])-np.min(data[i]))
    return data

if __name__ == "__main__":
    data = np.load('split_classes_fixed_size.npz')
    x_rd_cars = data['x_rd_cars']
    x_rd_bicycles = data['x_rd_bicycles']
    x_rd_pedestrians = data['x_rd_pedestrians']

    x_cps_cars = data['x_cps_cars']
    x_cps_bicycles = data['x_cps_bicycles']
    x_cps_pedestrians = data['x_cps_pedestrians']

    print(f'car - sequences: {len(x_rd_cars)}')
    print(f'bicycle - sequences: {len(x_rd_bicycles)}')
    print(f'pedestrian - sequences: {len(x_rd_pedestrians)}')

    # ----------------------------------------------------------------------------
    # balanced_dataset:
    l = 208
    dataset_name = 'tensorflow_dataset_balanced'
    x = np.append(x_rd_cars[:l], x_rd_bicycles[:l], axis=0)
    x = np.append(x, x_rd_pedestrians[:l], axis=0)
    x_cps = x_cps_cars[:l, :, 0]
    x_cps = np.append(x_cps, x_cps_bicycles[:l, :, 0], axis=0)
    x_cps = np.append(x_cps, x_cps_pedestrians[:l, :, 0], axis=0)
    y = np.ones((l,)) * 3  # (SparseCategoricalCrossentropy)
    y = np.append(y, np.ones((l,)) * 2, axis=0)
    y = np.append(y, np.ones((l,)) * 1, axis=0)

    if prepare_2d_data:
        x     = x.reshape(-1,input_shape[1],input_shape[2])
        x_cps = x_cps.reshape(-1)
        y     = y.repeat(input_shape[0])
        dataset_name = "tf_dataset_c1_b"

    if normalize_en:
        x     = normalize(x)

    x_cps = tf.cast(x_cps, dtype=tf.int32)
    dataset = tf.data.Dataset.from_tensor_slices(((x, x_cps), y)).shuffle(len(x), reshuffle_each_iteration=False)
    print(dataset.element_spec)
    tf.data.experimental.save(dataset, dataset_name)
    # ----------------------------------------------------------------------------
    # unbalanced_dataset:
    dataset_name = 'tensorflow_dataset_unbalanced'
    x = np.append(x_rd_cars, x_rd_bicycles, axis=0)
    x = np.append(x, x_rd_pedestrians, axis=0)
    x_cps = x_cps_cars[:, :, 0]
    x_cps = np.append(x_cps, x_cps_bicycles[:, :, 0], axis=0)
    x_cps = np.append(x_cps, x_cps_pedestrians[:, :, 0], axis=0)

    y = np.ones((len(x_rd_cars),)) * 3  # (SparseCategoricalCrossentropy)
    y = np.append(y, np.ones((len(x_rd_bicycles),)) * 2, axis=0)
    y = np.append(y, np.ones((len(x_rd_pedestrians),)) * 1, axis=0)

    if prepare_2d_data:
        x     = x.reshape(-1,input_shape[1],input_shape[2])
        x_cps = x_cps.reshape(-1)
        y     = y.repeat(input_shape[0])
        dataset_name = "tf_dataset_c1_ub"

    if normalize_en:
        x     = normalize(x)

    x_cps = tf.cast(x_cps, dtype=tf.int32)
    dataset = tf.data.Dataset.from_tensor_slices(((x, x_cps), y)).shuffle(len(x), reshuffle_each_iteration=False)
    print(dataset.element_spec)
    tf.data.experimental.save(dataset, dataset_name)
    print('done :)')
