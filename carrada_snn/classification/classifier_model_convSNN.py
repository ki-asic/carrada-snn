from tensorflow.keras import Model, layers
from tensorflow.keras import optimizers
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from neurons import RecurrentLifNeuronCell, IntegratorOutputNeuron
from conv_neurons import Conv2DLifCell
import tensorflow as tf
from classifier_dataset import input_shape


def get_model():
    input_rd = layers.Input(shape=(input_shape[0],  # Time
                                   input_shape[1]*input_shape[2]), name="input_rd_binary")
    inputs_conv = tf.keras.layers.Reshape((input_shape[0], input_shape[1], input_shape[2], 1))(input_rd)
    mid_z = layers.RNN(Conv2DLifCell(
        6, (3, 3),
        tau=5,
    ), return_sequences=True, name='conv_LIF_01')(inputs_conv)
    mid_z = layers.TimeDistributed(layers.MaxPool2D(padding='same'))(mid_z)
    mid_z = layers.RNN(Conv2DLifCell(
        16, (3, 3),
        tau=5,
    ), return_sequences=True, name='conv_LIF_02')(mid_z)
    mid_z = layers.TimeDistributed(layers.MaxPool2D(padding='same'))(mid_z)
    mid_z = layers.TimeDistributed(layers.Flatten())(mid_z)
    mid_z, v = layers.RNN(RecurrentLifNeuronCell(
        mid_z.shape[-1],  # number of inputs (dependent on prev. layer)
        32,  # number of neurons
        tau=5,
    ), return_sequences=True, name='LIF_recurrent_01')(mid_z)

    output_class, v = layers.RNN(IntegratorOutputNeuron(
        mid_z.shape[-1],  # number of input neurons from previous layer
        3,    # number of classes
    ), return_sequences=False, name='output_class')(mid_z)

    model = Model(
        inputs=[input_rd],
        outputs=[output_class],)

    opt = optimizers.Adam()
    model.compile(
        loss={'output_class': SparseCategoricalCrossentropy()},
        metrics={'output_class': 'accuracy'},
        optimizer=opt, run_eagerly=True)
    return model

