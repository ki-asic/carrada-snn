import math
from functools import reduce

class Conv2D(object):
    def __init__(self,
                   input_shape,
                   filters,
                   kernel_size,
                   strides=(1, 1),
                   padding='valid',
                   ):
        self.input_shape = input_shape
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.padding = padding

    def output_shape(self):
        if self.padding == "valid":
            out_width = (self.input_shape[0] - self.kernel_size[0]) // self.strides[0] + 1
            out_height = (self.input_shape[1] - self.kernel_size[1]) // self.strides[1] + 1
            return (out_width, out_height, self.filters)
        elif self.padding == "same":
            return (self.input_shape[0], self.input_shape[1], self.filters)
        else:
            raise Exception("Wrong value for padding")
    def mac_ops(self):
        out_width, out_height, out_channels = self.output_shape()
        mac = out_width*out_height*out_channels*self.kernel_size[0]*self.kernel_size[1]*self.input_shape[-1]
        return mac

class MaxPooling2D(object):
    def __init__(self,
                   input_shape,
                   pool_size=(2,2),
                   strides=None,
                   padding='valid',
                   ):
        self.input_shape = input_shape
        self.pool_size = pool_size
        if strides is None:
            strides = pool_size
        self.strides = strides
        self.padding = padding

    def output_shape(self):
        if self.padding == "valid":
            out_width = math.floor((self.input_shape[0]- self.pool_size[0]) / self.strides[0]) + 1
            out_height = math.floor((self.input_shape[1]- self.pool_size[1]) / self.strides[1]) + 1
            return (out_width, out_height, self.input_shape[-1])
        elif self.padding == "same":
            out_width = math.floor((self.input_shape[0] - 1) / self.strides[0]) + 1
            out_height = math.floor((self.input_shape[1] - 1) / self.strides[1]) + 1
            return (out_width, out_height, self.input_shape[-1])
        else:
            raise Exception("Wrong value for padding")

class Flatten(object):
    def __init__(self, input_shape):
        self.input_shape = input_shape

    def output_shape(self):
        return reduce(lambda x,y: x*y, self.input_shape)

class Dense(object):
    def __init__(self, input_shape, units):
        assert(isinstance(input_shape, int))
        self.input_shape = input_shape
        self.units = units

    def output_shape(self):
        return self.units

    def mac_ops(self):
        return self.units * self.input_shape

class LSTM(object):
    def __init__(self, input_shape, units):
        assert(isinstance(input_shape, int))
        self.input_shape = input_shape
        self.units = units

    def output_shape(self):
        return self.units

    def mac_ops(self):
        # only consider dense operations including weights
        return (self.units + self.input_shape) * self.units * 4

def model_2DConv():
    mac_ops = []

    input_shape = (26, 20, 1)  # (Y, X, channels)
    c1 = Conv2D(input_shape, 6, (3, 3),padding='valid')
    print (c1.mac_ops())
    mac_ops.append(c1.mac_ops())

    p1 = MaxPooling2D(c1.output_shape(),padding='same')
    print(p1.output_shape())

    c2 = Conv2D(p1.output_shape(), 16, (3, 3),padding='valid')
    print (c2.mac_ops())
    mac_ops.append(c2.mac_ops())

    p2 = MaxPooling2D(c2.output_shape(),padding='same')
    print(p2.output_shape())
    
    f1 = Flatten(p2.output_shape())
    print(f1.output_shape())

    d1 = Dense(f1.output_shape(), 30)
    print(d1.mac_ops())
    mac_ops.append(d1.mac_ops())

    d2 = Dense(d1.output_shape(), 30)
    print(d2.mac_ops())
    mac_ops.append(d2.mac_ops())

    d3 = Dense(d2.output_shape(), 3)
    print(d3.mac_ops())
    mac_ops.append(d3.mac_ops())

    print(mac_ops)
    print("MACs CNN", sum(mac_ops))

def model_LSTM():
    mac_ops = []

    n_frames = 8
    input_shape = (26, 20, 1)  # (Y, X, channels)
    c1 = Conv2D(input_shape, 6, (3, 3),padding='valid')
    print (c1.mac_ops())
    mac_ops.append(c1.mac_ops())

    p1 = MaxPooling2D(c1.output_shape(),padding='same')
    print(p1.output_shape())

    c2 = Conv2D(p1.output_shape(), 16, (3, 3),padding='valid')
    print (c2.mac_ops())
    mac_ops.append(c2.mac_ops())

    p2 = MaxPooling2D(c2.output_shape(),padding='same')
    print(p2.output_shape())
    
    f1 = Flatten(p2.output_shape())
    print(f1.output_shape())

    d1 = LSTM(f1.output_shape(), 32)
    print(d1.mac_ops())
    mac_ops.append(d1.mac_ops())

    d2 = Dense(d1.output_shape(), 3)
    print(d2.mac_ops())
    mac_ops.append(d2.mac_ops())

    print(mac_ops)
    print("MACs LSTM", sum(mac_ops)*n_frames)

def model_SNN():
    mac_ops = []

    n_frames = 8
    input_shape = (26, 20, 1)  # (Y, X, channels)
    c1 = Conv2D(input_shape, 6, (3, 3),padding='valid')
    print (c1.mac_ops())
    mac_ops.append(c1.mac_ops())

    print(mac_ops)
    print("MACs SNN", sum(mac_ops)*n_frames)

if __name__ == "__main__":
    model_2DConv()
    model_LSTM()
    model_SNN()
