"""
To extract small sequences containing the same object within a region
"""
import numpy as np
import carrada_dataset
import sys


def _get_frame_target_regions(target: int, labels, boxes, target_shape, frame):
    """
    list of regions containing all objects of the target class within a frame

    target: object class id -> 1,2,3
    labels: label class id
    boxes:
    """
    target_regions = []
    for i, label in enumerate(labels):
        if label == target:
            box = boxes[i]
            cx = box[0] + int((box[2] - box[0]) / 2)  # x of center point in ground truth
            cy = box[1] + int((box[3] - box[1]) / 2)  # y of center point in ground truth
            dx = int(target_shape[0] / 2)  # dx size of target box
            dy = int(target_shape[1] / 2)  # dy size of target box
            x_min, x_max, y_min, y_max = 0, np.shape(frame)[0], 0, np.shape(frame)[1]
            x_u, x_o, y_u, y_o = 0, target_shape[0], 0, target_shape[1]
            if cx - dx >= 0:
                x_min = cx - dx
            else:
                x_u = cx - dx
            if cx + dx <= np.shape(frame)[0]:
                x_max = cx + dx
            else:
                x_o = cx + dx - np.shape(frame)[0]
                x_o = target_shape[0]-x_o
            if cy - dy >= 0:
                y_min = cy - dy
            else:
                y_u = cy - dy
            if cy + dy <= np.shape(frame)[1]:
                y_max = cy + dy
            else:
                y_o = cy + dy - np.shape(frame)[1]
                y_o = target_shape[1] - y_o
            frame_data = frame[x_min:x_max, y_min:y_max]
            zero_padded_data = np.zeros(target_shape)
            zero_padded_data[-x_u:x_o, -y_u:y_o] = frame_data

            target_regions.append([zero_padded_data, [cx - dx, cx + dx, cy - dy, cy + dy]])
    return target_regions

def _get_frame_target_regions_v2(target: int, labels, boxes, target_shape, frame):
    """
    list of regions containing all objects of the target class within a frame

    target:         object class id -> 1,2,3
    labels:         label class id
    boxes:          (rg_min, dp_min, rg_max, dp_max)
    target_shape:   roi size (rg, dp)
    chen: in case ROI out of the frame boundary, we apply a tiny range displacement or doppler circular mapping instead of zero padding, to ensure every pixel in ROI is from the frame.
    """
    target_regions = []
    ny, nx = frame.shape
    for i, label in enumerate(labels):
        if label == target:
            box = boxes[i]
            cy = box[0] + int((box[2] - box[0]) / 2)  # y of center point in ground truth (rg)
            cx = box[1] + int((box[3] - box[1]) / 2)  # x of center point in ground truth (dp)
            dy = int(target_shape[0] / 2)  # dy size of target box
            dx = int(target_shape[1] / 2)  # dx size of target box
            x_min, x_max, y_min, y_max = 0, nx, 0, ny
            if(cy-dy < y_min):
                roi_bottom = 0
                roi_top    = target_shape[0]
            elif(cy+dy > y_max):
                roi_bottom = ny - target_shape[0]
                roi_top    = ny
            else:
                roi_bottom = cy-dy
                roi_top    = cy+dy

            roi_left   = cx-dx
            roi_right  = cx+dx
            if(roi_left < x_min):
                frame_dp_expand = np.hstack((frame,frame))
                roi = frame_dp_expand[roi_bottom:roi_top,roi_left+nx:roi_right+nx]
            elif(roi_right > x_max):
                frame_dp_expand = np.hstack((frame,frame))
                roi = frame_dp_expand[roi_bottom:roi_top,roi_left:roi_right]
            else:
                roi = frame[roi_bottom:roi_top,roi_left:roi_right]

            target_regions.append([roi, [roi_bottom,roi_top,roi_left,roi_right]])
    return target_regions

def _append_to_tracked_region(frame_region, tracked_region):
    """
    check if the frame region should be appended to a tracked region
    if its appended return True
    if not: return False

    frame_region[0] -> [[data_x], [data_y]]
    frame_region[1] -> [x_min, x_max, y_min, y_max]

    tracked_regions -> list of already tracked previous frame_regions
    """
    f_x_min, f_x_max = frame_region[1][0], frame_region[1][1]
    f_y_min, f_y_max = frame_region[1][2], frame_region[1][3]
    # tracked_region is already a list of regions, so check only the last one
    last_region = tracked_region[-1]
    t_x_min, t_x_max = last_region[1][0], last_region[1][1]
    t_y_min, t_y_max = last_region[1][2], last_region[1][3]
    if not (f_x_max < t_x_min or f_x_min > t_x_max or f_y_max < t_y_min or f_y_min > t_y_max):
        tracked_region.append(frame_region)
        return True
    return False


def _append_to_subsequences(sub_seqs, sub_seqs_cps, tracked_regions):
    """

    """
    for tracked_region in tracked_regions:
        subsequence = []
        cps = []  # center points
        for frame in tracked_region:
            subsequence.append(frame[0])
            cps.append(frame[1])
        sub_seqs.append(subsequence)
        sub_seqs_cps.append(cps)
    return sub_seqs, sub_seqs_cps


def get_labeled_subsequences(frames, annotations, target=3, shape=(20, 10)):
    """
    creates a list of sequences where a region around objects
    of the target-class is tracked over multiple frames
    """
    sub_seqs = []  # list with all subsequences
    sub_seqs_cps = []  # list of center points for all subsequences
    prev_frame_index = 0
    tracked_regions = []  # list of regions that are tracked to build sequences, may be appended to sub_seqs

    for frame_index, frame in enumerate(frames):
        try:
            annotation = annotations[f'{frame_index:06d}']
        except KeyError:
            continue
        boxes = annotation['boxes']
        labels = annotation['labels']

        # as annotations are used to iterate over frames the frames don't need to be sequential
        # if the frames are not directly following on each other, the tracked_regions will be cleared completely
        if frame_index != prev_frame_index + 1:
            sub_seqs, sub_seqs_cps = _append_to_subsequences(sub_seqs, sub_seqs_cps, tracked_regions)
            tracked_regions = []
        frame_regions = _get_frame_target_regions_v2(target, labels, boxes, shape, frame)
        # check if the target region belongs to an object already tracked:
        for frame_region in frame_regions:
            any_appended = False
            for i in range(len(tracked_regions)):
                tracked_region = tracked_regions[i]
                if _append_to_tracked_region(frame_region, tracked_region):
                    any_appended = True
                else:
                    sub_seqs, sub_seqs_cps = _append_to_subsequences(sub_seqs, sub_seqs_cps, [tracked_regions.pop(i)])
            if not any_appended:
                # the actual region in the frame does not belong to already traced region
                tracked_regions.append([frame_region])
        prev_frame_index = frame_index

    # append all tracked regions after all annotations are used
    sub_seqs, sub_seqs_cps = _append_to_subsequences(sub_seqs, sub_seqs_cps, tracked_regions)
    return sub_seqs, sub_seqs_cps


def get_fixed_length(subsequences, cps, length: int = 10):
    fixed_length_subsequences = []
    fixed_length_cps = []
    for k, subsequence in enumerate(subsequences):
        cp = cps[k]
        if len(subsequence) < length:
            continue
        fixed_length_subsequence = []
        fixed_length_cp = []
        for i, frame in enumerate(subsequence):
            if i % length != 0:
                fixed_length_subsequence.append(frame)
                fixed_length_cp.append(cp[i])
            else:
                if len(fixed_length_subsequence) == length:
                    fixed_length_subsequences.append(fixed_length_subsequence)
                    fixed_length_cps.append(fixed_length_cp)
                fixed_length_subsequence = [frame]
                fixed_length_cp = [cp[i]]
    return np.array(fixed_length_subsequences), np.array(fixed_length_cps)


sequences = carrada_dataset.get_sequences()
x_rd_cars, x_rd_pedestrians, x_rd_bicycles = [], [], []
x_cps_cars, x_cps_pedestrians, x_cps_bicycles = [], [], []
from classifier_dataset import input_shape
for i, sequence in enumerate(sequences):
    sys.stdout.write(f'\r processing sequence: {i+1}/{len(sequences)}')
    sys.stdout.flush()
    # load range-doppler and range-angle sequences:
    rd_s = carrada_dataset.load_range_doppler_frames(sequence)
    # ra_s = carrada_dataset.load_range_angle_frames(sequence)
    # load range-doppler and range-angle annotations:
    rd_a = carrada_dataset.load_range_doppler_annotations_box(sequence)
    # ra_a = carrada_dataset.load_range_angle_annotations_box(sequence)
    length = input_shape[0]
    subsequences, cps = get_labeled_subsequences(rd_s, rd_a, target=3, shape=input_shape[1:])
    fl_subsequences, fl_cps = get_fixed_length(subsequences, cps, length=length)
    x_rd_cars.extend(fl_subsequences)
    x_cps_cars.extend(fl_cps)

    subsequences, cps = get_labeled_subsequences(rd_s, rd_a, target=2, shape=input_shape[1:])
    fl_subsequences, fl_cps = get_fixed_length(subsequences, cps, length=length)
    x_rd_bicycles.extend(fl_subsequences)
    x_cps_bicycles.extend(fl_cps)

    subsequences, cps = get_labeled_subsequences(rd_s, rd_a, target=1, shape=input_shape[1:])
    fl_subsequences, fl_cps = get_fixed_length(subsequences, cps, length=length)
    x_rd_pedestrians.extend(fl_subsequences)
    x_cps_pedestrians.extend(fl_cps)


x_rd_cars = np.array(x_rd_cars)
x_rd_bicycles = np.array(x_rd_bicycles)
x_rd_pedestrians = np.array(x_rd_pedestrians)
x_cps_cars = np.array(x_cps_cars)
x_cps_bicycles = np.array(x_cps_bicycles)
x_cps_pedestrians = np.array(x_cps_pedestrians)

np.savez('split_classes_fixed_size',
         x_rd_cars=x_rd_cars, x_rd_bicycles=x_rd_bicycles, x_rd_pedestrians=x_rd_pedestrians,
         x_cps_cars=x_cps_cars, x_cps_bicycles=x_cps_bicycles, x_cps_pedestrians=x_cps_pedestrians)

print('\n done?')
