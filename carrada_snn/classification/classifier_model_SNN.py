from tensorflow.keras import Model, layers
from tensorflow.keras import optimizers
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from neurons import RecurrentLifNeuronCell, IntegratorOutputNeuron, LifNeuronCell
import tensorflow as tf
from classifier_dataset import input_shape


def get_model():
    input_rd = layers.Input(shape=(input_shape[0],  # Time
                                   input_shape[1]*input_shape[2]), name="input_rd")
    mid_z, v = layers.RNN(RecurrentLifNeuronCell(
        input_shape[1]*input_shape[2],  # number of inputs (dependent on dataset/encoding)
        15,  # number of neurons
        tau=5,
    ), return_sequences=True, name='LIF_recurrent_01')(input_rd)
    mid_z = tf.keras.layers.Dropout(0.1)(mid_z)
    mid_z, v = layers.RNN(RecurrentLifNeuronCell(
        15,  # number of inputs (dependent on prev. layer)
        15,  # number of neurons
        tau=5,
    ), return_sequences=True, name='LIF_recurrent_02')(mid_z)
    mid_z = tf.keras.layers.Dropout(0.2)(mid_z)
    output_class, v = layers.RNN(IntegratorOutputNeuron(
        15,  # number of input neurons from previous layer
        3,    # number of classes
    ), return_sequences=False, name='output_class')(mid_z)

    model = Model(
        inputs=[input_rd],
        outputs=[output_class],)

    opt = optimizers.Adam()
    model.compile(
        loss={'output_class': SparseCategoricalCrossentropy()},
        metrics={'output_class': 'accuracy'},
        optimizer=opt,)
    return model

