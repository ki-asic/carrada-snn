import os
import datetime
import tensorflow as tf
import classifier_model_3DConv
import classifier_model_2DConv
import classifier_model_LSTM
import classifier_model_LMU
import classifier_model_ConvLSTM
import numpy as np
import random
import uuid
from tensorboard.plugins.hparams import api as hp
import classifier_dataset
from tf_profile import get_flops
'''
we collect latest validation accuracy of all models here for quick overview
                   small ROI(18,10)        large ROI(26,20)
conv2D             81%                     87.4%
conv2D (norm.)                             90.5%
conv3D                                     88.9%
conv3D (norm. range dep.)                  89.5%
LSTM                                       93.2%
LSTM (norm.)                               94.7%
LSTM (norm. range dep.)                    90.5%
convLSTM                                   83.7%
convLSTM (norm.)                           92.1%
LMU
LMU (norm.range dep.)                      86.3%
SNN_15_15 (norm.range dep.)                85.2%
SNN_15_15                                  85.8%
convSNN                                    85.8%

LSTM - spike input                         86.3%
convSNN - real valued input                87.3%
convSNN - real valued input (norm)         93.6%
'''

dataset_path = 'tensorflow_dataset_unbalanced'
# dataset_path = 'tensorflow_dataset_balanced'
# dataset_path = 'tf_dataset_c1_ub'
batch_size = 32
epochs = 100


train_dataset, val_dataset = classifier_dataset.get_dataset(dataset_path, batch_size)

# --------------------------------------------
# train model on spiking dataset:
"""
train_dataset, val_dataset = classifier_dataset.get_spiking_dataset(dataset_path, batch_size)
input_shape = classifier_dataset.input_shape


def reshape(x, y):
    x = tf.reshape(x, (-1, input_shape[0], input_shape[1], input_shape[2]))  # (Time, Y, X)
    x = tf.expand_dims(x, axis=-1)
    return x, y


train_dataset = train_dataset.map(reshape)
val_dataset = val_dataset.map(reshape)
"""
# --------------------------------------------

def train(log_dir, hparams):
    tf.random.set_seed(hparams[SEED])
    random.seed(hparams[SEED])
    os.environ['PYTHONHASHSEED'] = str(hparams[SEED])
    np.random.seed(hparams[SEED])

    # model = classifier_model_3DConv.get_model()
    # model = classifier_model_2DConv.get_model()
    model = classifier_model_LSTM.get_model()
    # model = classifier_model_ConvLSTM.get_model()
    # model = classifier_model_LMU.get_model()

    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1, profile_batch=0)
    callbacks = [tensorboard_callback]

    model.summary()
    get_flops(model)
    print("###############################")

    model.fit(
        train_dataset, validation_data=val_dataset,
        batch_size=batch_size, epochs=epochs,
        callbacks=callbacks, )

    _, accuracy = model.evaluate(val_dataset, batch_size=batch_size)

    with tf.summary.create_file_writer(log_dir).as_default():
        hp.hparams(hparams)
        tf.summary.scalar(METRIC_ACCURACY, accuracy, step=1)


experiment_dir = 'logdir_' + datetime.datetime.now().strftime("%Y%m%d%H%M%S")
os.mkdir(experiment_dir)
with open(experiment_dir + '/dataset.txt', 'w') as f:
    f.write(dataset_path)

SEED = hp.HParam('seeds', hp.Discrete([42, 1337, 123, 666, 1000, 7, 948436, 13, 1408, 12, 69, 111111]))
METRIC_ACCURACY = 'accuracy'

with tf.summary.create_file_writer(experiment_dir).as_default():
    hp.hparams_config(hparams=[SEED],
                      metrics=[hp.Metric(METRIC_ACCURACY, display_name='Accuracy'), ],)

for seed in SEED.domain.values:
    hparams = {SEED: seed}
    run_name = str(uuid.uuid4())
    print('--- Starting trial: %s' % run_name)
    print({h.name: hparams[h] for h in hparams})
    run_dir = experiment_dir + '/' + run_name
    train(run_dir, hparams)

print('done :)')
