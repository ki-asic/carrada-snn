"""
some helper functions to handle the CARRADA Dataset
"""
from pathlib import Path
import cv2
import numpy as np
import json

# Where is the Dataset located at your machine?
root = Path('..')/'Carrada'

classes = {
    1: 'pedestrian',
    2: 'bicycle',
    3: 'car'
}


def get_sequences():
    seqs = open(root / 'validated_seqs.txt').readlines()
    ret_seqs = []
    for seq in seqs:
        ret_seqs.append(seq.strip())
    return ret_seqs


def load_camera_frames(seq_path):
    camera_frames = []
    for file in sorted((root / seq_path / 'camera_images').iterdir()):
        if not file.suffix == '.jpg':
            continue
        data = cv2.imread(str(file), cv2.IMREAD_COLOR)
        camera_frames.append(data)
    return np.array(camera_frames)


def load_range_angle_frames(seq_path):
    frames = []
    for file in sorted((root / seq_path / 'range_angle_numpy').iterdir()):
        data = np.load(str(file))
        frames.append(data)
    return np.array(frames)


def load_range_doppler_frames(seq_path):
    frames = []
    for file in sorted((root / seq_path / 'range_doppler_numpy').iterdir()):
        data = np.load(str(file))
        frames.append(data)
    return np.array(frames)


def load_range_doppler_annotations_box(seq_path):
    path = root / seq_path / 'annotations' / 'box' / 'range_doppler_light.json'
    with open(path) as f:
        labels = json.load(f)
    return labels

def load_range_doppler_annotations_sparse(seq_path):
    frames = []
    for file in sorted((root / seq_path / 'annotations' / 'sparse').iterdir()): 
        data = np.load(str(file/'range_doppler.npy'))
        frames.append(data)
    return np.array(frames)

def load_range_angle_annotations_box(seq_path):
    path = root / seq_path / 'annotations' / 'box' / 'range_angle_light.json'
    with open(path) as f:
        labels = json.load(f)
    return labels
