'''
TU Dresden
single frame object classification on rd map
'''
from tensorflow.keras import Model, layers
from tensorflow.keras import optimizers
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import tensorflow as tf
from classifier_dataset import input_shape


l = list(input_shape)
l.pop(0)
l.append(1)
input_shape = tuple(l)


def get_model():
    input_rd = layers.Input(shape=input_shape, name="input_rd")
    x = layers.Conv2D(6, (3, 3),padding='valid',activation='relu')(input_rd)
    x = layers.MaxPooling2D(padding='same')(x)
    x = layers.Conv2D(16, (3, 3),padding='valid',activation='relu')(x)
    x = layers.MaxPooling2D(padding='same')(x)
    x = layers.Flatten()(x)
    x = layers.Dense(30,activation='relu')(x)
    x = layers.Dense(30,activation='relu')(x)
    output_class = layers.Dense(3, activation='softmax', name='output_class')(x)

    model = Model(
        inputs=[input_rd],
        outputs=[output_class],)

    opt = optimizers.Adam()
    model.compile(
        loss={'output_class': SparseCategoricalCrossentropy()},
        metrics={'output_class': 'accuracy'},
        optimizer=opt,)
    return model

