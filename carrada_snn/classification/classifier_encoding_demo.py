import numpy as np
import matplotlib.pyplot as plt
import classifier_dataset


dataset_path = 'tensorflow_dataset_unbalanced'
batch_size = 1

train_dataset, val_dataset = classifier_dataset.get_spiking_dataset(dataset_path, batch_size)

data = train_dataset.cache()

for sample in data:
    x = np.array(sample[0][0])
    y = int(sample[1][0])
    plt.imshow(x.T, aspect='auto', interpolation='none')
    plt.title(f'class: {y}')
    plt.show()

