from tensorflow.keras import Model, layers
from tensorflow.keras import optimizers
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import tensorflow as tf
from classifier_dataset import input_shape
import keras_lmu
# https://www.nengo.ai/keras-lmu/basic-usage.html#basic-usage

l = list(input_shape)
l.append(1)
input_shape = tuple(l)

def get_model():
    input_rd = layers.Input(shape=input_shape, name="input_rd")
    x = layers.TimeDistributed(layers.Conv2D(6, (3, 3), padding='valid', activation='relu'))(input_rd)
    x = layers.TimeDistributed(layers.MaxPooling2D(padding='same'))(x)
    x = layers.TimeDistributed(layers.Conv2D(16, (3, 3), padding='valid', activation='relu'))(x)
    x = layers.TimeDistributed(layers.MaxPooling2D(padding='same'))(x)
    x = layers.TimeDistributed(layers.Flatten())(x)
    x = tf.keras.layers.RNN(keras_lmu.LMUCell(
        memory_d=1,
        order=256,
        theta=320,
        hidden_cell=tf.keras.layers.SimpleRNNCell(212),
        hidden_to_memory=False,
        memory_to_memory=False,
        input_to_hidden=True,
        # kernel_initializer="ones",
    ))(x)
    output_class = layers.Dense(4, activation='softmax', name='output_class')(x)

    model = Model(
        inputs=[input_rd],
        outputs=[output_class],)

    opt = optimizers.Adam()
    model.compile(
        loss={'output_class': SparseCategoricalCrossentropy()},
        metrics={'output_class': 'accuracy'},
        optimizer=opt,)
    return model

