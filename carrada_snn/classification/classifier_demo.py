import numpy as np
import visualization
import matplotlib.pyplot as plt


scatter_optimization = False


def get_scatters(cps, x_data, r_scale=0.196):
    scatter_x = []
    scatter_y = []
    for seq_id, cps_seq in enumerate(cps):
        data_seq = x_data[seq_id]
        for frame_id, frame_cps in enumerate(cps_seq):
            frame = data_seq[frame_id]
            for x_idx in range(np.shape(frame)[0]):
                for y_idx in range(np.shape(frame)[1]):
                    r = (frame_cps[0] + x_idx) # * r_scale
                    amplitude = frame[x_idx, y_idx]
                    v_rel_idx = frame_cps[2] + y_idx
                    if scatter_optimization:
                        if v_rel_idx == 32:
                            amplitude = 0
                        amplitude -= 0
                        if amplitude <= 0:
                            amplitude = 0

                    scatter_y.append(amplitude)
                    scatter_x.append(r)
    return scatter_x, scatter_y


data = np.load('split_classes_fixed_size.npz')
x_rd_cars = data['x_rd_cars']
x_rd_bicycles = data['x_rd_bicycles']
x_rd_pedestrians = data['x_rd_pedestrians']

print(f'car - sequences: {len(x_rd_cars)}')
print(f'bicycle - sequences: {len(x_rd_bicycles)}')
print(f'pedestrian - sequences: {len(x_rd_pedestrians)}')

# ---------------------------------------------------------------------------------------------
# value histogram:
# complete_data = np.append(x_rd_cars, x_rd_bicycles, axis=0)
# complete_data = np.append(complete_data, x_rd_pedestrians, axis=0)
#
# plt.hist(complete_data.flatten(), 1000, density=True)
# plt.show()
# range dependency:
x_cps_cars = data['x_cps_cars']
x_cps_bicycles = data['x_cps_bicycles']
x_cps_pedestrians = data['x_cps_pedestrians']
# maybe split the range dependency over classes as the means over the amplitudes between
# cars and pedestrians are different at the same range?
x = list(range(256))
m = -0.3
n = 65
cut_y = 38
threshold = np.array([m*value + n for value in x])
threshold[threshold < cut_y] = cut_y


stacked_plot = True
thickness = 0.01
alpha = 0.3
if stacked_plot:
    scatter_x, scatter_y = get_scatters(x_cps_cars, x_rd_cars)
    plt.scatter(scatter_x, scatter_y, thickness, alpha=0.3)
    scatter_x, scatter_y = get_scatters(x_cps_bicycles, x_rd_bicycles)
    plt.scatter(scatter_x, scatter_y, thickness, alpha=0.2)
    scatter_x, scatter_y = get_scatters(x_cps_pedestrians, x_rd_pedestrians)
    plt.scatter(scatter_x, scatter_y, thickness, alpha=0.1)
else:
    fig, axs = plt.subplots(3, sharex='all', sharey='all')
    fig.suptitle('Amplitudes over range?')
    scatter_x, scatter_y = get_scatters(x_cps_cars, x_rd_cars)
    axs[0].scatter(scatter_x, scatter_y, thickness)
    scatter_x, scatter_y = get_scatters(x_cps_bicycles, x_rd_bicycles)
    axs[1].scatter(scatter_x, scatter_y, thickness)
    scatter_x, scatter_y = get_scatters(x_cps_pedestrians, x_rd_pedestrians)
    axs[2].scatter(scatter_x, scatter_y, thickness)


#r = list(range(256))
#threshold = []
#for distance in r:
#    threshold.append(cal_threshold(distance))
plt.plot(x, threshold, color='red')
plt.show()


# ---------------------------------------------------------------------------------------------
start_idx = 90
end_idx = 110
visualization.parallel_show_subsequences(
    [x_rd_cars[start_idx:end_idx], x_rd_bicycles[start_idx:end_idx], x_rd_pedestrians[start_idx:end_idx]],
    ['cars', 'bicycles', 'pedestrians'])


