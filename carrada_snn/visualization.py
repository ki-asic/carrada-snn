import numpy as np
import cv2  # pip install opencv-contrib-python
from pynput.keyboard import Key, Listener  # pip install pynput
import time
import matplotlib.pyplot as plt
import matplotlib.patches as patches

_stop = False
_frame_idx = 0


def on_press(key):
    global _stop
    global _frame_idx
    if key == Key.space:
        _stop = not _stop
    if key == Key.left and _stop:
        _frame_idx -= 1
    if key == Key.right and _stop:
        _frame_idx += 1


_listener = Listener(on_press=on_press)
_listener.start()


def wait(t=30):
    cv2.waitKey(t) & 0xFF


def rescale(image, scale_percent):
    width = int(image.shape[1] * scale_percent)
    height = int(image.shape[0] * scale_percent)
    dim = (width, height)
    return cv2.resize(image, dim, interpolation=cv2.INTER_AREA)


def parallel_show_subsequences(subsequences: list, titles: list):
    time_length = np.shape(subsequences[0])[1]
    global _frame_idx
    _frame_idx = 0
    try:
        while 1:
            if _frame_idx == time_length:
                _frame_idx = 0
            for i, subsequence in enumerate(subsequences):
                title = titles[i]
                frame = subsequence[:, _frame_idx, :, :]
                parallel_show_frames(frame, title=title)
            if _stop:
                stop_frame = _frame_idx
                print(f'frame: {_frame_idx}')
                while _stop:
                    if _frame_idx != stop_frame:
                        break
                    else:
                        time.sleep(0.001)
                    wait(500)
            else:
                _frame_idx += 1
                wait(500)
    except IndexError:
        pass


def parallel_show_frames(subsequence_frames, scale_percent=6, title: str = 'undefined'):
    shape = np.shape(subsequence_frames)
    fill = np.ones((shape[0], shape[1], 2))
    subsequence_frames = np.append(subsequence_frames, fill, axis=-1)
    image = np.concatenate((subsequence_frames[:]), axis=-1)
    image = image[:, :].astype(np.uint8)
    image = rescale(image, scale_percent)
    cv2.imshow(title, image)


def show_rd(range_doppler_frame,
            range_doppler_annotation=None,
            annotation_index=None,
            scale_percent=2):

    rgb_grayscale = range_doppler_frame
    range_doppler_frame = np.expand_dims(range_doppler_frame, -1)
    range_doppler_frame[:, :, 0] = rgb_grayscale
    range_doppler_frame = np.append(range_doppler_frame, np.expand_dims(rgb_grayscale, -1), axis=-1)
    range_doppler_frame = np.append(range_doppler_frame, np.expand_dims(rgb_grayscale, -1), axis=-1)
    rd = range_doppler_frame[:, :, :].astype(np.uint8)
    width = int(rd.shape[1] * scale_percent)
    height = int(rd.shape[0] * scale_percent)
    dim = (width, height)
    rd = cv2.resize(rd, dim, interpolation=cv2.INTER_AREA)
    try:
        annotation = range_doppler_annotation[f'{annotation_index:06d}']
        boxes = annotation['boxes']
        labels = annotation['labels']
        for i, label in enumerate(labels):
            box = boxes[i]
            start_point = (box[1] * scale_percent, box[0] * scale_percent)
            end_point = (box[3] * scale_percent, box[2] * scale_percent)
            color = (255, 0, 0)
            thickness = 2
            rd = cv2.rectangle(rd, start_point, end_point, color, thickness)
    except:
        pass
    cv2.imshow("RangeDoppler", rd)


def show_ra(range_angle_frame,
            range_angle_annotation=None,
            annotation_index=None,
            scale_percent=2):
    rgb_grayscale = range_angle_frame
    range_angle_frame = np.expand_dims(range_angle_frame, -1)
    range_angle_frame[:, :, 0] = rgb_grayscale
    range_angle_frame = np.append(range_angle_frame, np.expand_dims(rgb_grayscale, -1), axis=-1)
    range_angle_frame = np.append(range_angle_frame, np.expand_dims(rgb_grayscale, -1), axis=-1)
    ra = range_angle_frame[:, :, :].astype(np.uint8)
    width = int(ra.shape[1] * scale_percent)
    height = int(ra.shape[0] * scale_percent)
    dim = (width, height)
    ra = cv2.resize(ra, dim, interpolation=cv2.INTER_AREA)
    try:
        annotation = range_angle_annotation[f'{annotation_index:06d}']
        boxes = annotation['boxes']
        labels = annotation['labels']
        for i, label in enumerate(labels):
            box = boxes[i]
            start_point = (box[1] * scale_percent, box[0] * scale_percent)
            end_point = (box[3] * scale_percent, box[2] * scale_percent)
            color = (255, 0, 0)
            thickness = 2
            ra = cv2.rectangle(ra, start_point, end_point, color, thickness)
    except:
        pass
    cv2.imshow("RangeAngle", ra)


def show_full_sequence(
        camera_frames,
        range_doppler_frames,
        range_angle_frames,
        range_doppler_annotations,
        range_angle_annotations):
    global _frame_idx
    _frame_idx = 0
    try:
        while 1:
            cv2.imshow('camera', camera_frames[_frame_idx])
            show_rd(range_doppler_frames[_frame_idx], range_doppler_annotations, annotation_index=_frame_idx)
            show_ra(range_angle_frames[_frame_idx], range_angle_annotations, annotation_index=_frame_idx)
            if _stop:
                stop_frame = _frame_idx
                print(f'frame: {_frame_idx}')
                while _stop:
                    if _frame_idx != stop_frame:
                        break
                    else:
                        time.sleep(0.001)
                    cv2.waitKey(30) & 0xFF
            else:
                _frame_idx += 1
                cv2.waitKey(30) & 0xFF
    except IndexError:
        pass


def _show_bbox(frame_name:str, labels, boxes, frame):
    '''
    visualize bbox in each frame for debugging purpose
    '''
    class_name = {1:"P",2:"B",3:"C"}
    plt.figure(figsize=(9,9))
    plt.imshow(frame)
    ax = plt.gca()
    plt.title(frame_name)
    for i in range(len(labels)):
        label = labels[i]
        box = boxes[i]
        cy = box[0] + int((box[2] - box[0]) / 2)  # y of center point in ground truth
        cx = box[1] + int((box[3] - box[1]) / 2)  # x of center point in ground truth
        bbox=patches.Rectangle((box[1],box[0]),box[3] - box[1],box[2] - box[0],edgecolor='r',facecolor='none')
        ax.add_patch(bbox)
        plt.text(cx, cy, class_name[label], horizontalalignment='center',fontdict={'size': 12, 'color': 'b'})
    plt.show()




