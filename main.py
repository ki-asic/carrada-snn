#!/usr/bin/env python3
"""
Main description of the module.
"""
# Standard libraries
import argparse
import json
import logging
import pathlib
import time
# Local libraries
import carrada_snn.cfar.eval_spiking_cfar


def parse_args():
    """
    Obtain the simulation options from the input arguments
    """
    parser = argparse.ArgumentParser(
        usage="main.py [-m {CFAR classif}]"
    )
    parser.add_argument("-m", "--method", type=str, choices=["CFAR", "classif"],
                        default="CFAR", metavar="",
                        help="{CFAR | classif} algorithm to be run on the data"
                       )

    # Get the values from the argument list
    args = parser.parse_args()
    method = args.method
    return method


def load_config():
    """
    Load the configuration file with the simulation parameters
    """
    # Load configuaration data from local file
    cfar_conf_file = "config/cfar_experiment.json"
    with open(cfar_conf_file) as f:
        cfar_config = json.load(f)
    # os_cfar_args = cfar_config["cfar_args"]["{}D".format(dims)]
    return cfar_config


def conf_logger():
    # Create log folder
    logpath = pathlib.Path(__file__).resolve().parent.joinpath("log")
    pathlib.Path(logpath).mkdir(parents=True, exist_ok=True)
    datetime = time.strftime("%Y-%m-%d %H:%M:%S")
    fdatetime = time.strftime("%Y%m%d-%H%M%S")
    # Create logger
    logger = logging.getLogger('Carrada-SNN')
    logger.setLevel(logging.DEBUG)

    # Create file handler
    file_handler = logging.FileHandler("{}/{}.log".format(logpath, fdatetime))
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s',
                                  "%H:%M:%S")
    file_handler.setFormatter(formatter)
    file_handler.stream.write("{} MAIN PROGRAM EXECUTION\n".format(datetime))
    logger.addHandler(file_handler)

    # Create console handler
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    return logger

def run(method):
    cfar_config = load_config()
    if method=="CFAR":

        #carrada_snn.cfar.eval_spiking_cfar.find_good_cfar_params()
        #carrada_snn.cfar.eval_spiking_cfar.plot_cfar_and_annotations()
        #carrada_snn.cfar.eval_spiking_cfar.comparison_experiment(**cfar_config)
        #carrada_snn.cfar.eval_spiking_cfar.compare_os_cfar_implementations(**cfar_config)
        #carrada_snn.cfar.eval_spiking_cfar.analyze_data_quantization()

        #for cfar in ["os_cfar", "ca_cfar"]:
        #for cfar in ["ca_cfar"]:
        for cfar in ["os_cfar_db", "os_cfar"]:
            #f_results = "carrada_snn/cfar/cfar_results/{}_round_down_performance2.npz".format(cfar)
            f_results = "carrada_snn/cfar/cfar_results/{}_performance3.npz".format(cfar)
            carrada_snn.cfar.eval_spiking_cfar.performance_experiment(cfar,
                    f_results, **cfar_config)
            f_plot= "carrada_snn/cfar/cfar_results/{}_performance3.pdf".format(cfar)
            carrada_snn.cfar.eval_spiking_cfar.plot_performance_experiment(f_results,f_plot)
    elif method=="classif":
        #TODO: Classification entry point
        pass
    else:
        raise ValueError("Incorrect method")


def main():
    logger = conf_logger()
    init_message = "Running carrada-snn"
    logger.info(init_message)

    method = parse_args()
    run(method)


if __name__ == "__main__":
    main()
