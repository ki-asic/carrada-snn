# Spiking neural networks for radar processing with [CARRADA dataset](https://github.com/valeoai/carrada_dataset)  

Python code for automotive radar processing with spiking neural networks (SNN) applied to the CARRADA dataset.
The repository currently includes:
- **Convenience functions to load parts of the CARRADA dataset**, e.g., all range Doppler maps and the various annotations
- **Range-Doppler ROI sub-dataset for object classification:** Automatic extraction of sub-dataset with region-of-interests (ROI) around detected objects in range-Doppler maps for object classification. Both single single ROIs (one per object per frame) and sequences of the ROIs from the same object are supported.
- **Train SNNs and DNNs using TensorFlow for object classification in ROI dataset.**
- **Object detection with spiking CFAR:** Implementations of two CFAR (constant false alarm rate) algorithms for detection of radar targets in range Doppler maps using spiking neural networks. Ordered-statistic CFAR (OS-CFAR) and cell-averaging (CA-CFAR).

## License

The code is in this repository is licensed under the MIT License. See `LICENSE` for details.

## Citation

If you use the code for a scientific publication, please cite our [article in Frontiers of Neuromorphic Engineering](https://www.frontiersin.org/articles/10.3389/fnins.2022.851774/full):

```
@ARTICLE{10.3389/fnins.2022.851774,
AUTHOR={Vogginger, Bernhard and Kreutz, Felix and López-Randulfe, Javier and Liu, Chen and Dietrich, Robin and Gonzalez, Hector A. and Scholz, Daniel and Reeb, Nico and Auge, Daniel and Hille, Julian and Arsalan, Muhammad and Mirus, Florian and Grassmann, Cyprian and Knoll, Alois and Mayr, Christian},   
TITLE={Automotive Radar Processing With Spiking Neural Networks: Concepts and Challenges},      
JOURNAL={Frontiers in Neuroscience},      
VOLUME={16},      
YEAR={2022},      
URL={https://www.frontiersin.org/article/10.3389/fnins.2022.851774},       
DOI={10.3389/fnins.2022.851774},      
ISSN={1662-453X}
}
```



## Prerequesites for loading the CARRADA dataset

0. After downloading the CARRADA dataset there are no annotations. Follow the instructions 
provided with the dataset to run the annotation-pipeline

1. check 'carrada_dataset.py' and modify the root-path of the Dataset according to your setup. 
The default path assumes the dataset is located at the parent directory of this script in the 
folder 'CARADAR_Dataset'  

2. run 'carrada_info.py', if the dataset is correctly loaded you should see some output like:

    Number of Scenarios: 30  
    labeled-frames: 7193  
    pedestrian-examples: 3239  
    bicycle-examples: 1770  
    car-examples: 3741  
    
### have a look on the data
run 'demo.py' and 3 windows should show up (maybe you need to click on them in the taskbar, 
if the windows are not in the foreground). Hit the spacebar-key on you keyboard to pause the frame-sequence, 
while paused use the arrow keys (left/right) to scroll manually through the frames.

If you get the following error:
> cv2.error: OpenCV(3.4.2) /tmp/build/80754af9/opencv-suite_1535558553474/work/modules/highgui/src/window.cpp:632: error: (-2:Unspecified error) The function is not implemented. Rebuild the library with Windows, GTK+ 2.x or Carbon support.
>   If you are on Ubuntu or Debian, install libgtk2.0-dev and pkg-config, then re-run cmake or configure script in function 'cvShowImage' 

[This](https://stackoverflow.com/questions/40207011/opencv-not-working-properly-with-python-on-linux-with-anaconda-getting-error-th/51013434#51013434) it what actually solves the problem


### step by step introduction for classification

1. run 'classifier_data_extraction.py' the script will extract sequences of single
objects from the whole dataset and saves the extracted sequences in some file 'split_classes_fixed_size.npz'

2. (optional) check how the Data is looking like: run 'classifier_demo.py'  
You can see an "animation" of some selected samples from the Dataset.
You can pause the animation by hitting the spacebar-key on your keyboard,
if the animation is paused use the arrow keys to manually scroll through the frames

3. run 'classifier_dataset' this script will crate two tensorflow-datasets out of the previously
created .npz-file.  
The Datasets are named 'tensorflow_dataset_balanced' and 'tensorflow_dataset_unbalanced'
indicating that there are the same amount of samples in 'tensorflow_dataset_balanced' for each class.

4. run 'classifier_train.py' to train a 3DConv-model defined in 'classifier_model.py' or a SNN-model defined in 'classifier_snn_model.py'.
There will be multiple runs with random weight initializations, to get a feeling on how strong the influence of
different initializations is on the accuracy.

5. (optional) to run conv2D model, a macro "prepare_2d_data" in `classifier_dataset.py` should be set to True to refresh the dataset. 

6. with finished training a tensorboard logdir is created, something like: "logdir_20210204154342" 
you can copy the name of this logdir and paste it within "plots.py" in the list named 'experiments'.
If you run the script "plots.py" it should show a plot summarizing the val_accuracy of the runs.
Alternatively load the logdir in tensorboard.

