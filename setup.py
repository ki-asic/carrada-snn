#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name="carrada-snn",
    version="0.1",
    description="Package for implementing SNNs on automotive radar signals",
    url="https://gitlab.com/ki-asic/carrada-snn/",
    author="KI-ASIC Consortium",
    install_requires=[
        "numpy>=1.16",
        "matplotlib>=3.1.2",
        "numba>=0.51.2",
        "sklearn",
        "opencv-python>=4.5.1"
        "pynput>=1.7.3"
    ],
)
